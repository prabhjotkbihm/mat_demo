<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('url', 'language'));
        $this->load->model('Property_model');

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
        
	}

	/**
	 * Redirect if needed, otherwise display the user list
	 */
	
	public function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		// else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		// {
		// 	// redirect them to the home page because they must be an administrator to view this
		// 	return show_error('You must be an administrator to view this page 2.');
		// }
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

            // $this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
            redirect('dashboard', 'refresh');
		}
	}

	/**
	 * Log the user in
	 */
	public function login()
	{
        if(null == $this->input->get('ref')){
            //$property_slug = $_GET[]//$this->input->get('rf');
            $redirect_to = 'dashboard';

            
        }else{
            $redirect_to = $this->input->get('ref');
        }
        if ($this->ion_auth->logged_in())
		{
            $this->session->set_flashdata('flsh_msg', 'You are already logged in.');
			// redirect them to the login page
			redirect($redirect_to, 'refresh');
		}
		$this->data['title'] = $this->lang->line('login_heading');

		// validate form input
		$this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
		$this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');

		if ($this->form_validation->run() === TRUE)
		{
			// check to see if the user is logging in
			// check for "remember me"
			$remember = (bool)$this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                $set_role_in_session = $this->ion_auth->set_role_in_session();
                if($set_role_in_session == 'ask_for_role_choose'){
                    $this->session->set_flashdata('message', 'Please select a role you want to login.');
				    redirect('auth/select_role', 'refresh');
                }else if($set_role_in_session ==  'role_set_in_session'){
                    //if($this->ion_auth->is_profile_complete()){ //Disable due to we dont need check payment method is active or not for host user. 
                        redirect($redirect_to, 'refresh');
                    //}
                }else{
                    redirect($redirect_to, 'refresh');
                }
            }
			else
			{
				// if the login was un-successful
				// redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			// the user is not logging in so display the login page
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'class'=> 'form-control',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id' => 'password',
				'class'=> 'form-control',
				'type' => 'password',
			);

			$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'login', $this->data);
		}
	}

	/**
	 * Log the user out
	 */
	public function logout()
	{
		$this->data['title'] = "Logout";

		// log the user out
		$logout = $this->ion_auth->logout();

		// redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
        // redirect('auth/login', 'refresh');
		redirect(base_url(), 'refresh');
        
	}

	/**
	 * Change password
	 */
	public function change_password()
	{
		$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() === FALSE)
		{
			// display the form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
			$this->data['old_password'] = array(
				'name' => 'old',
				'id' => 'old',
                'type' => 'password',
                'class'=> 'form-control',
			);
			$this->data['new_password'] = array(
				'name' => 'new',
				'id' => 'new',
				'type' => 'password',
                'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                'class'=> 'form-control',
			);
			$this->data['new_password_confirm'] = array(
				'name' => 'new_confirm',
				'id' => 'new_confirm',
				'type' => 'password',
                'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                'class'=> 'form-control',
			);
			$this->data['user_id'] = array(
				'name' => 'user_id',
				'id' => 'user_id',
				'type' => 'hidden',
				'value' => $user->id,
			);

            // render
			$this->data['user_details'] = $this->ion_auth->user()->row();
                         
            $this->data['martial_arts_names'] = $this->ion_auth->get_array_martial_arts_name();
            
			$this->data['martial_arts_details'] = $this->ion_auth->get_martial_arts_details_of_user();
			if(!empty($this->data['martial_arts_details'])){
				$this->data['martial_arts_style'] = $this->ion_auth->get_array_martial_arts_style($this->data['martial_arts_details'][0]->martial_art_name_id);
			}else{
				$this->data['martial_arts_style'] = array();
			}
			

            $this->data['title'] = 'Profile';

        $this->load->view('dashboard/header', $this->data);
          
        $this->load->view('auth/change_password');
      
       $this->load->view('dashboard/footer');

			// $this->_render_page('auth' . DIRECTORY_SEPARATOR . 'change_password', $this->data);
		}
		else
		{
			$identity = $this->session->userdata('identity');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
				//if the password was successfully changed
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                
				$this->logout();
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
                // redirect('auth/change_password', 'refresh');
                $this->session->set_flashdata('flsh_msg', 'Error while update password.');
                //$this->session->set_flashdata('alert', 'Add Images and events to properties.');
                $this->session->set_flashdata('tab_pane', 'security');
                redirect('profile', 'refresh');
			}
		}
	}

	/**
	 * Forgot password
	 */
	public function forgot_password()
	{
		// setting validation rules by checking whether identity is username or email
		if ($this->config->item('identity', 'ion_auth') != 'email')
		{
			$this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
		}
		else
		{
			$this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}


		if ($this->form_validation->run() === FALSE)
		{
			$this->data['type'] = $this->config->item('identity', 'ion_auth');
			// setup the input
			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity',
				'class' => 'form-control',
			);

			if ($this->config->item('identity', 'ion_auth') != 'email')
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			// set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'forgot_password', $this->data);
		}
		else
		{
			$identity_column = $this->config->item('identity', 'ion_auth');
			$identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

			if (empty($identity))
			{

				if ($this->config->item('identity', 'ion_auth') != 'email')
				{
					$this->ion_auth->set_error('forgot_password_identity_not_found');
				}
				else
				{
					$this->ion_auth->set_error('forgot_password_email_not_found');
				}

				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			}

			// run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{
				// if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			}
		}
	}

	/**
	 * Reset password - final step for forgotten password
	 *
	 * @param string|null $code The reset code
	 */
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			// if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() === FALSE)
			{
				// display the form

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id' => 'new',
                    'type' => 'password',
                    'class'=> 'form-control',
					'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id' => 'new_confirm',
                    'type' => 'password',
                    'class'=> 'form-control',
					'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
				);
				$this->data['user_id'] = array(
					'name' => 'user_id',
					'id' => 'user_id',
					'type' => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				// render
				$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'reset_password', $this->data);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					// something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						// if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						redirect("auth/login", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('auth/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			// if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	/**
	 * Activate the user
	 *
	 * @param int         $id   The user ID
	 * @param string|bool $code The activation code
	 */
	public function activate($id, $code = FALSE)
	{
		if ($code !== FALSE)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
            $activation = $this->ion_auth->activate($id);
            if ($activation)
            {
                // redirect them to the auth page
                $this->session->set_flashdata('flsh_msg', $this->ion_auth->messages());
                redirect("SuperAdmin/switch_to_user", 'refresh');
            }else
            {
                // redirect them to the forgot password page
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect("auth/forgot_password", 'refresh');
            }
		}

		if ($activation)
		{
			// redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth", 'refresh');
		}
		else
		{
			// redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	/**
	 * Deactivate the user
	 *
	 * @param int|string|null $id The user ID
	 */
	public function deactivate($id = NULL)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page 1.');
		}

		$id = (int)$id;

		$this->load->library('form_validation');
		$this->form_validation->set_rulespassword('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
		$this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

		// if ($this->form_validation->run() === FALSE)
		// {
		// 	// insert csrf check
		// 	$this->data['csrf'] = $this->_get_csrf_nonce();
		// 	$this->data['user'] = $this->ion_auth->user($id)->row();

		// 	$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'deactivate_user', $this->data);
		// }
		// else
		// {
		// 	// do we really want to deactivate?
		// 	if ($this->input->post('confirm') == 'yes')
		// 	{
				// do we have a valid request?
				// if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
				// {
				// 	return show_error($this->lang->line('error_csrf'));
				// }

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
				{
                    $this->session->set_flashdata('flsh_msg', 'Account Deactivated');
					$this->ion_auth->deactivate($id);
				}
			// }

			// redirect them back to the auth page
			redirect('SuperAdmin/switch_to_user', 'refresh');
		// }
	}

	/**
	 * Create a new user
	 */
	public function create_user()
	{
        if ($this->ion_auth->logged_in())
		{
            $this->session->set_flashdata('flsh_msg', 'You are already logged in.');
			// redirect them to the login page
			redirect('dashboard', 'refresh');
		}
        $this->data['title'] = 'Create Profile';
        

		// if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		// {
		// 	redirect('auth', 'refresh');
		// }

        
		$tables = $this->config->item('tables', 'ion_auth');
		$identity_column = $this->config->item('identity', 'ion_auth');
		$this->data['identity_column'] = $identity_column;

		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'trim|required');
		$this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'trim|required');
		if ($identity_column !== 'email')
		{
			$this->form_validation->set_rules('identity', $this->lang->line('create_user_validation_identity_label'), 'trim|required|is_unique[' . $tables['users'] . '.' . $identity_column . ']');
			$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email');
		}
		else
		{
			$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email|is_unique[' . $tables['users'] . '.email]');
		}
		$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
		$this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim');
		$this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

		if ($this->form_validation->run() === TRUE)
		{
			$email = strtolower($this->input->post('email'));
			$identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
			$password = $this->input->post('password');

			$additional_data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'company' => $this->input->post('company'),
				//'phone' => $this->input->post('phone'),
            );
            $groups = $this->input->post('groups');
            if($groups == ''){
                $groups = array('0'=>3);
            }
                //echo "<pre>"; print_r($groups);echo "</pre>";die('saddd');
            $martialartsnames = $this->input->post('martial_arts_names');
            
        }
       
		if ($this->form_validation->run() === TRUE && $this->ion_auth->register($identity, $password, $email, $additional_data, $groups, $martialartsnames))
		{
			// check to see if we are creating the user
            // redirect them back to the admin page
            
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth/login", 'refresh');
		}
		else
		{
			// display the create user form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['first_name'] = array(
				'name' => 'first_name',
				'id' => 'first_name',
				'class'=>'form-control',
				'type' => 'text',
				'value' => $this->form_validation->set_value('first_name'),
			);
			$this->data['last_name'] = array(
				'name' => 'last_name',
				'id' => 'last_name',
				'class'=>'form-control',
				'type' => 'text',
				'value' => $this->form_validation->set_value('last_name'),
			);
			$this->data['identity'] = array(
				'name' => 'identity',
				'id' => 'identity',
				'class'=>'form-control',
				'type' => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['email'] = array(
				'name' => 'email',
				'id' => 'email',
				'class'=>'form-control',
				'type' => 'text',
				'value' => $this->form_validation->set_value('email'),
			);
			$this->data['company'] = array(
				'name' => 'company',
				'id' => 'company',
				'class'=>'form-control',
				'type' => 'text',
				'value' => $this->form_validation->set_value('company'),
			);
			$this->data['phone'] = array(
				'name' => 'phone',
				'id' => 'phone',
				'class'=>'form-control',
				'type' => 'text',
				'value' => $this->form_validation->set_value('phone'),
            );
            
            // $this->data['register_as'] = array(
			// 	'2' => 'Host',
			// 	'3' => 'Guest'
            // );
            // $this->data['martial_arts_names'] = array(
			// 	'1' => 'MA1',
			// 	'2' => 'MA2'
            // );

            $this->data['register_as'] = $this->ion_auth->get_array_groups();
            $this->data['martial_arts_names'] = $this->ion_auth->get_array_martial_arts_name();
            $this->data['country_names'] = $this->ion_auth->get_array_of_country();
            
           
			$this->data['password'] = array(
				'name' => 'password',
				'id' => 'password',
				'class'=>'form-control',
				'type' => 'password',
				'title' => 'minimum 8 characters',
				'value' => $this->form_validation->set_value('password'),
			);
			$this->data['password_confirm'] = array(
				'name' => 'password_confirm',
				'id' => 'password_confirm',
				'class'=>'form-control',
				'type' => 'password',
				'value' => $this->form_validation->set_value('password_confirm'),
			);

			$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'create_user', $this->data);
		}
	}
	/**
	* Redirect a user checking if is admin
	*/
	public function redirectUser(){
		if ($this->ion_auth->is_admin()){
			redirect('auth', 'refresh');
		}
		redirect('/', 'refresh');
	}

	/**
	 * Edit a user
	 *
	 * @param int|string $id
	 */
	public function edit_user($id)
	{
		$this->data['title'] = $this->lang->line('edit_user_heading');

		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('auth', 'refresh');
		}

		$user = $this->ion_auth->user($id)->row();
		$groups = $this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'trim|required');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'trim|required');
		$this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'trim|required');
		//$this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'trim|required');

		if (isset($_POST) && !empty($_POST))
		{
			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				//show_error($this->lang->line('error_csrf'));
			}

			// update the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}

			if ($this->form_validation->run() === TRUE)
			{
				$data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					// 'company' => $this->input->post('company'),
                    'phone' => $this->input->post('phone'),
                    'street_address'=>$this->input->post('street_address'),
                    'country'=>$this->input->post('country'),
                    'state'=>$this->input->post('state'),
                    'city'=>$this->input->post('city'),
                    'gender'=>$this->input->post('gender'),
                    'marital_status'=>$this->input->post('marital_status'),
                    'zip_code'=>$this->input->post('zip_code'),
                    'd_o_b'=>$this->change_date_format($this->input->post('d_o_b')),
                    'suburb'=>$this->input->post('suburb'),
                    'modified_by' =>  $this->input->post('user_id'),
                    'modified_by_admin' =>  $this->input->post('admin_id'),  
				);
                
				// update the password if it was posted
				if ($this->input->post('password'))
				{
					$data['password'] = $this->input->post('password');
				}

				// Only allow updating groups if user is admin
				if ($this->ion_auth->is_admin())
				{
					// Update the groups user belongs to
					$groupData = $this->input->post('groups');

					if (isset($groupData) && !empty($groupData))
					{

						$this->ion_auth->remove_from_group('', $id);

						foreach ($groupData as $grp)
						{
							$this->ion_auth->add_to_group($grp, $id);
						}

					}
				}

				// check to see if we are updating the user
				if ($this->ion_auth->update($user->id, $data))
				{
					// redirect them back to the admin page if admin, or to the base url if non admin
					$this->session->set_flashdata('message', $this->ion_auth->messages());
                    // $this->redirectUser();
                    $this->session->set_flashdata('flsh_msg', 'User Updated Successfully');
                    redirect('profile','refresh');
                    //redirect('profile_edit/'.$user->id, 'refresh');

				}
				else
				{
					// redirect them back to the admin page if admin, or to the base url if non admin
					$this->session->set_flashdata('message', $this->ion_auth->errors());
                    // $this->redirectUser();
                    $this->session->set_flashdata('flsh_msg', 'Error while update user.');
                    
                    redirect('profile_edit/'.$user->id, 'refresh');

				}

			}
		}

		// display the edit user form
		//$this->data['csrf'] = $this->_get_csrf_nonce();

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;

        $this->data['country_names'] = $this->ion_auth->get_array_of_country();

		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
            'type'  => 'text',
            'class'=> 'form-control',
            'required' => 'required',
			'value' => $this->form_validation->set_value('first_name', $user->first_name),
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
            'type'  => 'text',
            'class'=> 'form-control',
            'required' => 'required',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
		);
		$this->data['street_address'] = array(
			'name'  => 'street_address',
			'id'    => 'street_address',
            'type'  => 'text',
            'class'=> 'form-control',
			'value' => $this->form_validation->set_value('street_address', $user->street_address),
        );
        // $this->data['city'] = array(
		// 	'name'  => 'city',
		// 	'id'    => 'city',
        //     'type'  => 'text',
        //     'class'=> 'form-control',
		// 	'value' => $this->form_validation->set_value('city', $user->city),
        // );
        // $this->data['state'] = array(
		// 	'name'  => 'state',
		// 	'id'    => 'state',
        //     'type'  => 'text',
        //     'class'=> 'form-control',
		// 	'value' => $this->form_validation->set_value('state', $user->state),
        // );
        // $this->data['country'] = array(
		// 	'name'  => 'country',
		// 	'id'    => 'country',
        //     'type'  => 'text',
        //     'class'=> 'form-control',
		// 	'value' => $this->form_validation->set_value('country', $user->country),
		// );
		
        // );
        $this->data['marital_status'] = array(
			'name'  => 'marital_status',
            'id'    => 'marital_status',
            'class'=> 'form-control',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('marital_status', $user->marital_status),
        );
        $this->data['gender'] = array(
			'name'  => 'gender',
            'id'    => 'gender',
            'class'=> 'form-control',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('gender', $user->gender),
        );
        $this->data['zip_code'] = array(
			'name'  => 'zip_code',
            'id'    => 'zip_code',
            'class'=> 'form-control',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('zip_code', $user->zip_code),
        );
        $this->data['phone'] = array(
			'name'  => 'phone',
            'id'    => 'phone',
            'class'=> 'form-control',
            'type'  => 'text',
            'required' => 'required',
			'value' => $this->form_validation->set_value('phone', $user->phone),
        );
        
        $this->data['d_o_b'] = array(
			'name'  => 'd_o_b',
            'id'    => 'd_o_b',
            'class'=> 'form-control datepicker',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('d_o_b', date("d F, Y", strtotime($user->d_o_b))),
        );
        
		// $this->data['password'] = array(
		// 	'name' => 'password',
        //     'id'   => 'password',
        //     'class'=> 'form-control',
		// 	'type' => 'password'
		// );
		// $this->data['password_confirm'] = array(
		// 	'name' => 'password_confirm',
        //     'id'   => 'password_confirm',
        //     'class'=> 'form-control',
		// 	'type' => 'password'
		// );

        // $this->_render_page('auth' . DIRECTORY_SEPARATOR . 'edit_user', $this->data);
        
        $this->data['user_details'] = $this->ion_auth->user()->row();

        $this->data['title'] = 'Edit Profile';

    $this->load->view('dashboard/header', $this->data);
      
    $this->load->view('auth/edit_user');
  
   $this->load->view('dashboard/footer');



	}

	/**
	 * Create a new group
	 */
	public function create_group()
	{
		$this->data['title'] = $this->lang->line('create_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'trim|required|alpha_dash');

		if ($this->form_validation->run() === TRUE)
		{
			$new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
			if ($new_group_id)
			{
				// check to see if we are creating the group
				// redirect them back to the admin page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth", 'refresh');
			}
		}
		else
		{
			// display the create group form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['group_name'] = array(
				'name'  => 'group_name',
				'id'    => 'group_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name'),
			);
			$this->data['description'] = array(
				'name'  => 'description',
				'id'    => 'description',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('description'),
			);

			$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'create_group', $this->data);
		}
	}

	/**
	 * Edit a group
	 *
	 * @param int|string $id
	 */
	public function edit_group($id)
	{
		// bail if no group id given
		if (!$id || empty($id))
		{
			redirect('auth', 'refresh');
		}

		$this->data['title'] = $this->lang->line('edit_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		$group = $this->ion_auth->group($id)->row();

		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');

		if (isset($_POST) && !empty($_POST))
		{
			if ($this->form_validation->run() === TRUE)
			{
				$group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_description']);

				if ($group_update)
				{
					$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
				}
				else
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
				}
				redirect("auth", 'refresh');
			}
		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$this->data['group'] = $group;

		$readonly = $this->config->item('admin_group', 'ion_auth') === $group->name ? 'readonly' : '';

		$this->data['group_name'] = array(
			'name'    => 'group_name',
			'id'      => 'group_name',
			'type'    => 'text',
			'value'   => $this->form_validation->set_value('group_name', $group->name),
			$readonly => $readonly,
		);
		$this->data['group_description'] = array(
			'name'  => 'group_description',
			'id'    => 'group_description',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_description', $group->description),
		);

		$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'edit_group', $this->data);
	}

	/**
	 * @return array A CSRF key-value pair
	 */
	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	/**
	 * @return bool Whether the posted CSRF token matches
	 */
	public function _valid_csrf_nonce(){
		$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
		if ($csrfkey && $csrfkey === $this->session->flashdata('csrfvalue')){
			return TRUE;
		}
			return FALSE;
	}

	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
    }
    



    /**
     * Custom functions starts
     */

    public function select_role()
	{
        if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
        $this->data['register_as'] = $this->ion_auth->get_array_groups();
        $this->_render_page('auth' . DIRECTORY_SEPARATOR . 'select_role', $this->data);
        
	}

    public function set_role(){
        if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
        $this->session->set_userdata('group_id',$this->input->post('select_role'));
        $this->ion_auth->add_role_to_user_if_not_exist($this->input->post('select_role'));
        if(isset($_REQUEST['set_as_default'])){
            $this->ion_auth->set_role_as_default($this->input->post('select_role'));
        }
        
        
       // if($this->ion_auth->is_profile_complete()){ //Disable due to we dont need check payment method is active or not for host user.
            redirect('dashboard', 'refresh');
       // }
    }
    
    public function set_role_directly(){
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
        $select_role = $this->uri->segment(3);
        $this->session->set_userdata('group_id',$select_role);
        $this->ion_auth->add_role_to_user_if_not_exist($select_role);
        
        //if($this->ion_auth->is_profile_complete()){
            redirect('dashboard', 'refresh');
        //}
    }

    public function select_payment(){
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
        $this->data['title'] = 'Select Payment Method';
         $payment_methods = $this->ion_auth->get_latest_payment_methods();
         
         foreach($payment_methods as $payment_method){
             if(!empty($payment_method)){
                 
                    
                    if($payment_method['payment_details'] !== ''){
                        $payment_details = json_decode($payment_method['payment_details'], true);
                        $payment_details['default_payment_method'] = '';
                        
                            $payment_details[$payment_method['payment_type'].'_default_payment_method'] = $payment_method['is_default'];
                        
                        //  $keyyy = $payment_method['payment_type'].'_modified_date';
                        // $this->data[$keyyy] =  'Last update on '.$this->change_date_format_with_time($payment_method['created_date']);
                    }
                    if(!empty($payment_details)){
                        foreach($payment_details as $payment_detail_key=>$payment_detail_val){
                            $this->data[$payment_detail_key] = $payment_detail_val;
                        }
                    }               
             }
         }
        // echo "<pre>";print_r($this->data);echo "</pre>";die('as');
         
       
        $this->load->view('dashboard/header', $this->data);
          
        $this->load->view('auth/select_payment');
      
       $this->load->view('dashboard/footer');
    }

    
    public function set_payment(){

        $payment_type = $this->uri->segment(3);
        
        $payment_details = json_encode($_REQUEST['payment_details']);
        ;
        if(isset($_REQUEST['is_default'])){
            $is_default = '1';
        }else{
            $is_default = '0';
        }
        
        if($this->ion_auth->add_payment_method_to_db($payment_type,$payment_details,$is_default)){
            $this->session->set_flashdata('flsh_msg', 'Payment Method Saved.');
        }else{
            $this->session->set_flashdata('flsh_msg', 'Payment Method Failed to Saved.');
        }
        if($payment_type == 'direct'){
            $this->session->set_flashdata('tab_pane', 'direct');
        }
        
        redirect('auth/select_payment', 'refresh');
        
      
	}
	

	public function edit_martial_arts(){
        // echo "<pre>"; print_r($this->input->post('martial_arts_names')); echo "</pre>";
        // echo "<pre>"; print_r($this->input->post('martial_arts_style')); echo "</pre>";die('sadfff');
		$result = $this->ion_auth->edit_martial_arts($this->input->post('martial_arts_names'),$this->input->post('martial_arts_style'));
        if($result){
			$this->session->set_flashdata('flsh_msg', 'Settings saved.');
			
			$this->session->set_flashdata('tab_pane', 'martial-arts');
            redirect('profile', 'refresh');
        }else{
			$this->session->set_flashdata('flsh_msg', 'Something went wrong.');
			
			$this->session->set_flashdata('tab_pane', 'martial-arts');
            redirect('profile', 'refresh');
		}
	}


	public function loadMartialArtsStyleData()
	{
		
		$loadId=$_REQUEST['loadId'];

		
		$result=$this->ion_auth->getData($loadId);
		$HTML="";
		//echo "<pre>";print_r($result->result());echo "</pre>";
		if($result->num_rows() > 0){
			foreach($result->result() as $list){
                if($_REQUEST['selected_martial_style_id'] == $list->id){
                    $HTML.="<option datavalue='".$list->id."' selected value='".$list->id."'>".$list->name."</option>";
                }else{
                    $HTML.="<option datavalue='".$list->id."' value='".$list->id."'>".$list->name."</option>";
                }
				
			}
		}
		echo $HTML;
	}


    public function edit_image_photo(){
        
        if(isset($_REQUEST['submit'])){
           
            $result_upload = 'empty';
  
            if($_FILES['image']['name'] !== ''){
                
                $result_upload = $this->do_upload_image();
            }

            $data['result_upload'] = $result_upload;
            
            $this->session->set_flashdata('flsh_msg', 'User image uploaded.');
            $this->session->set_flashdata('tab_pane', 'profile-image');
            redirect('profile','refresh');
        }
    }

    public function do_upload_image()
        {
            $rsult = array();
            $data_for_db = array();
            
                
                $config['upload_path']          = './uploads/avatars/';
                $config['allowed_types']        = 'jpg|jpeg|png|JPG|JPEG|PNG';
                $config['encrypt_name'] = TRUE;
                

                $this->load->library('upload', $config);

                

                if ( ! $this->upload->do_upload('image'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        $rsult[] = array('status'=>'error','data'=>$error);
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        
                        $property_data = array(
                            'image' => $data['upload_data']['file_name'],
                            'modified_by'=> $this->ion_auth->user()->row()->id,
                            'modified_by_admin'=> $this->ion_auth->admin_id()
                        );

                        $this->db->where('id', $this->ion_auth->user()->row()->id);
                        $this->db->update('users', $property_data);

                        $rsult = array('status'=>'success','data'=>$data);
                }

            
            
            return $rsult;
        }
  

        public function change_date_format($date){
            return date("Y-m-d", strtotime($date));
            
        }

        public function change_date_format_with_time($date){
            return date("d M, Y h:i:s A", strtotime($date));
            
        }


        public function get_option_name($table,$id){
            return $this->ion_auth->get_option_name($table,$id);
        }


        public function review_for_guest(){
            $this->data['title'] = 'Review for Guests';

            $this->data['my_clients'] = $this->ion_auth->get_my_clients();
            

            $this->load->view('dashboard/header', $this->data);
              
            $this->load->view('dashboard/host/review_for_guest');
          
           $this->load->view('dashboard/footer');
        }

        public function review_from_guest(){
            $this->data['title'] = 'Client Ratings';

            $this->data['my_ratings'] = $this->ion_auth->get_my_ratings_as_role('3'); //(3) = Get ratings by Guests
            
            $this->load->view('dashboard/header', $this->data);
              
            $this->load->view('dashboard/host/review_from_guest');
          
           $this->load->view('dashboard/footer');
        }


        public function currency_setting(){
            if (!$this->ion_auth->logged_in())
			{
				// redirect them to the login page
				//redirect('auth/login', 'refresh');
				show_404();
			}
			$this->data['title'] = 'Currency Conversion Tool';
			
			if(null !== $this->uri->segment(2)){
				$this->data['amt'] = $this->uri->segment(2);
				
			}
			 

            
            $this->load->view('auth/currency_setting', $this->data);
          
           
		}
		
		public function add_new_rating(){
			$to_user_id = $this->uri->segment(3);
			$by_user_id = $this->session->userdata('user_id');
			$comment_if_any = $this->input->post('comment_if_any');
			
			$non_calculated_rating = $_REQUEST['rating_critera_id'];
			$this->db->where('to_user_id', $to_user_id); 
			$this->db->where('by_user_id', $by_user_id); 
			$this->db->where('is_old','0');
			
			$query = $this->db->get('reviews_ratings');
			
			if ($query->num_rows() == 0)
			{ 
				if($this->ion_auth->add_new_rating($to_user_id,$by_user_id,$comment_if_any,$non_calculated_rating)){
					$this->session->set_flashdata('flsh_msg', 'Rating successfully added.');
				
					redirect('review_for_guest', 'refresh');
				}else{
					$this->session->set_flashdata('flsh_msg', 'Error while adding Rating.');
					
					redirect('review_for_guest', 'refresh');
				}

				
				
			}else{
				foreach($query->result_array() as $result_of_existing_reviews){
					$this->ion_auth->make_is_old_all_rating($result_of_existing_reviews['reviews_rating_id']);
				}
				
				if($this->ion_auth->add_new_rating($to_user_id,$by_user_id,$comment_if_any,$non_calculated_rating)){
					$this->session->set_flashdata('flsh_msg', 'Rating successfully added.');
				
					redirect('review_for_guest', 'refresh');
				}else{
					$this->session->set_flashdata('flsh_msg', 'Error while adding Rating.');
					
					redirect('review_for_guest', 'refresh');
				}

			}

			
        }






        public function test(){
            $this->data['title'] = 'test';
            
           
            $this->load->view('dashboard/header', $this->data);
              
            $this->load->view('auth/test');
          
           $this->load->view('dashboard/footer');
        }

        public function mailview(){
            $this->data['title'] = 'Mail View';
            $this->load->view('auth/test');       
        }
        public function add_meta_data(){
            return $this->ion_auth->add_meta_data();
        }



        public function switch_to_user_account(){
          if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
            {
                return show_error('You must be an administrator to view this page 2.');
            }else{
                if($this->ion_auth->login_by_super_admin($this->uri->segment(3))){
                    $redirect_to = 'dashboard';
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                    $set_role_in_session = $this->ion_auth->set_role_in_session();
                    if($set_role_in_session == 'ask_for_role_choose'){
                        $this->session->set_flashdata('message', 'Please select a role you want to login.');
                        redirect('auth/select_role', 'refresh');
                    }else if($set_role_in_session ==  'role_set_in_session'){
                        
                            redirect($redirect_to, 'refresh');
                        
                    }else{
                        redirect($redirect_to, 'refresh');
                    }
                }else{
                    return show_error('Something went wrong.');
                }
            }
            // echo "<pre>"; print_r($this->uri->segment(3));echo "</pre>";die('asd');
        }

        public function switch_to_su_account(){
            if ($this->session->userdata('is_super_admin') !== 1) // remove this elseif if you want to enable this for non-admins
              {
                  return show_error('You must be an administrator to view this page');
              }else{
                  if($this->ion_auth->login_by_super_admin($this->uri->segment(3))){
                      $redirect_to = 'dashboard';
                      $this->session->set_flashdata('message', $this->ion_auth->messages());
                      $set_role_in_session = $this->ion_auth->set_role_in_session();
                      if($set_role_in_session == 'ask_for_role_choose'){
                          $this->session->set_flashdata('message', 'Please select a role you want to login.');
                          redirect('auth/select_role', 'refresh');
                      }else if($set_role_in_session ==  'role_set_in_session'){
                          
                              redirect($redirect_to, 'refresh');
                          
                      }else{
                          redirect($redirect_to, 'refresh');
                      }
                  }else{
                      return show_error('Something went wrong.');
                  }
              }
              // echo "<pre>"; print_r($this->uri->segment(3));echo "</pre>";die('asd');
          }
          public function review_to_property(){
            $this->data['title'] = 'Review to Property';
            $this->data['my_properties'] = $this->ion_auth->get_my_properties();
            $this->load->view('dashboard/header', $this->data);
            $this->load->view('dashboard/guest/review_to_property');          
           $this->load->view('dashboard/footer');
        }

        public function review_to_host(){
            $this->data['title'] = 'Review to Hosts';

            $this->data['my_clients'] = $this->ion_auth->get_my_hosts();
            

            $this->load->view('dashboard/header', $this->data);
              
            $this->load->view('dashboard/guest/review_to_host');
          
           $this->load->view('dashboard/footer');
        }


        public function review_from_host(){
            $this->data['title'] = 'Review from hosts';

            $this->data['my_ratings'] = $this->ion_auth->get_my_ratings_as_role('2'); //(2) = Get ratings by Host
            
            $this->load->view('dashboard/header', $this->data);
              
            $this->load->view('dashboard/guest/review_from_host');
          
           $this->load->view('dashboard/footer');
        }



        public function add_new_rating_guest_to_host(){
			$to_user_id = $this->uri->segment(3);
			$by_user_id = $this->session->userdata('user_id');
			$comment_if_any = $this->input->post('comment_if_any');
			
			$non_calculated_rating = $_REQUEST['rating_critera_id'];
			$this->db->where('to_user_id', $to_user_id); 
			$this->db->where('by_user_id', $by_user_id); 
			$this->db->where('is_old','0');
			
			$query = $this->db->get('reviews_ratings');
			
			if ($query->num_rows() == 0)
			{ 
				if($this->ion_auth->add_new_rating($to_user_id,$by_user_id,$comment_if_any,$non_calculated_rating)){
					$this->session->set_flashdata('flsh_msg', 'Rating successfully added.');
				
					redirect('review_to_host', 'refresh');
				}else{
					$this->session->set_flashdata('flsh_msg', 'Error while adding Rating.');
					
					redirect('review_to_host', 'refresh');
				}

				
				
			}else{
				foreach($query->result_array() as $result_of_existing_reviews){
					$this->ion_auth->make_is_old_all_rating($result_of_existing_reviews['reviews_rating_id']);
				}
				
				if($this->ion_auth->add_new_rating($to_user_id,$by_user_id,$comment_if_any,$non_calculated_rating)){
					$this->session->set_flashdata('flsh_msg', 'Rating successfully added.');
				
					redirect('review_to_host', 'refresh');
				}else{
					$this->session->set_flashdata('flsh_msg', 'Error while adding Rating.');
					
					redirect('review_to_host', 'refresh');
				}

			}

			
        }
        public function add_new_rating_guest_to_property(){
			$to_property_id = $this->uri->segment(3);
			$property_added_by = $this->uri->segment(4);
			$to_user_id = $this->session->userdata('user_id');
			$comment_if_any = $this->input->post('comment_if_any');
			$non_calculated_rating = $_REQUEST['rating_critera_id'];
			$property_title = $this->input->post('property_title');
			$this->db->where('to_property_id', $to_property_id); 
			$this->db->where('by_user_id', $to_user_id); 
			$this->db->where('is_old','0');
			
			$query = $this->db->get('reviews_ratings');
			
			if ($query->num_rows() == 0)
			{ 
				if($this->ion_auth->add_new_rating_property($to_property_id,$to_user_id,$comment_if_any,$non_calculated_rating,$property_added_by,$property_title)){
					$this->session->set_flashdata('flsh_msg', 'Rating successfully added.');
				
					redirect('review_to_property', 'refresh');
				}else{
					$this->session->set_flashdata('flsh_msg', 'Error while adding Rating.');
					
					redirect('review_to_property', 'refresh');
				}

				
				
			}else{
				foreach($query->result_array() as $result_of_existing_reviews){
					$this->ion_auth->make_is_old_all_rating($result_of_existing_reviews['reviews_rating_id']);
				}
				
				if($this->ion_auth->add_new_rating_property($to_property_id,$to_user_id,$comment_if_any,$non_calculated_rating,$property_added_by,$property_title)){
					$this->session->set_flashdata('flsh_msg', 'Rating successfully added.');
				
					redirect('review_to_property', 'refresh');
				}else{
					$this->session->set_flashdata('flsh_msg', 'Error while adding Rating.');
					
					redirect('review_to_property', 'refresh');
				}

			}

			
        }

    /**
     * Custom functions ends
     */
}



<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuperAdmin extends CI_Controller {
    public function __construct()
	{
        parent::__construct();
        $this->load->database();
		
        $this->load->helper(array('url', 'language'));
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->model('Super_admin_model');
        $this->load->model('ion_auth_model');

		

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

    }
    /*admin files view*/
    
    public function cover_images(){
         if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
            $data['title'] = 'Property Cover Images';
            if($this->session->has_userdata('group_id')){
                $this->load->view('dashboard/header', $data);
                if($this->session->userdata['group_id'] == 1){ //host
                    $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
                     $data['cover_images'] = $this->Super_admin_model->get_cover_images();
                    $this->load->view('dashboard/admin/cover_images',$data);
                }else{ //admin

                    redirect('auth/select_role', 'refresh');
                }
                
                $this->load->view('dashboard/footer');
            }else{
                redirect('auth/select_role', 'refresh');
            }
    }
      public function gallery_images(){
         if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
            $data['title'] = 'Property Gallery Images';
            if($this->session->has_userdata('group_id')){
                $this->load->view('dashboard/header', $data);
                if($this->session->userdata['group_id'] == 1){ //host
                    $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
                   $data['gallery_images'] = $this->Super_admin_model->get_gallery_images();
                    $this->load->view('dashboard/admin/gallery_images',$data);
                }else{ //admin

                    redirect('auth/select_role', 'refresh');
                }
                
                $this->load->view('dashboard/footer');
            }else{
                redirect('auth/select_role', 'refresh');
            }
    }
      public function event_images(){
         if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
            $data['title'] = 'Property Event Images';
            if($this->session->has_userdata('group_id')){
                $this->load->view('dashboard/header', $data);
                if($this->session->userdata['group_id'] == 1){ //host
                    $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
                   $data['event_images'] = $this->Super_admin_model->get_event_images();
                    $this->load->view('dashboard/admin/event_images',$data);
                }else{ //admin

                    redirect('auth/select_role', 'refresh');
                }
                
                $this->load->view('dashboard/footer');
            }else{
                redirect('auth/select_role', 'refresh');
            }
    }
    public function main_description(){
         if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
            $data['title'] = 'Property Main Descriptions';
            if($this->session->has_userdata('group_id')){
                $this->load->view('dashboard/header', $data);
                if($this->session->userdata['group_id'] == 1){ //host
                    $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
                    $data['main_description'] = $this->Super_admin_model->get_main_description();
                    $this->load->view('dashboard/admin/main_description',$data);
                }else{ //admin

                    redirect('auth/select_role', 'refresh');
                }
                
                $this->load->view('dashboard/footer');
            }else{
                redirect('auth/select_role', 'refresh');
            }
    }   
    public function event_description(){
         if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
            $data['title'] = 'Property Event Descriptions';
            if($this->session->has_userdata('group_id')){
                $this->load->view('dashboard/header', $data);
                if($this->session->userdata['group_id'] == 1){ //host
                    $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
                    $data['event_description'] = $this->Super_admin_model->get_event_description();
                    $this->load->view('dashboard/admin/event_description',$data);
                }else{ //admin

                    redirect('auth/select_role', 'refresh');
                }
                
                $this->load->view('dashboard/footer');
            }else{
                redirect('auth/select_role', 'refresh');
            }
    }   
    public function  update_main_description_status(){
        if ($this->ion_auth->logged_in()){
            $id = $this->input->post('id');
            $approve = $this->input->post('approve');
            $field = $this->input->post('field');
            $field1 = $this->input->post('field1');
            $email = $this->input->post('email');
            $image = $this->input->post('image');
            $name = $this->input->post('name');
            $type = $this->input->post('type');
            if($this->Super_admin_model->update_main_description_status($id,$approve,$field,$field1,$email,$image,$name,$type)){
                echo "success";
            }else{
                echo "error";
            }  
        }else{
            echo "unauthorized";
        }
    }
    public function  update_gallary_status(){
        if ($this->ion_auth->logged_in()){
            $id = $this->input->post('id');
            $approve = $this->input->post('approve');
            $field = $this->input->post('field');
            $email = $this->input->post('email');
            $image = $this->input->post('image');
            $name = $this->input->post('name');
            if($this->Super_admin_model->update_gallery_status($id,$approve,$field,$email,$image,$name)){
                echo "success";
            }else{
                echo "error";
            }   
        }else{
            echo "unauthorized";
        }
    }
    public function  update_event_status(){
        if ($this->ion_auth->logged_in()){
            $id = $this->input->post('id');
            $approve = $this->input->post('approve');
            $field = $this->input->post('field');
            $field1 = $this->input->post('field1');
            $email = $this->input->post('email');
            $image = $this->input->post('image');
            $name = $this->input->post('name');
            $type = $this->input->post('type');
                if($this->Super_admin_model->update_event_status($id,$approve,$field,$field1,$email,$image,$name,$type)){
                    echo "success";
                }else{
                    echo "error";
                }   
        }else{
            echo "unauthorized";
        }
    }
    // public function  dashboard3(){
    //     if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
    //         $data['title'] = '';
    //         if($this->session->has_userdata('group_id')){
    //             $this->load->view('dashboard/header', $data);
    //             if($this->session->userdata['group_id'] == 1){ //admin
    //                 $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
    //             if(!empty($this->Super_admin_model->my_total_earning())){
    //                 $data['total_earnings_till_now'] = $this->Super_admin_model->my_total_earning();
    //             }
    //             else{
    //                 $data['total_earnings_till_now'] = '0';
    //             }
    //             if(!empty($this->Super_admin_model->my_total_earning('current'))){
    //                 $data['total_earnings_current_month'] = $this->Super_admin_model->my_total_earning('current');
    //             }
    //             else{
    //                 $data['total_earnings_current_month'] = '0';
    //             }
    //             if(!empty($this->Super_admin_model->my_total_earning('last'))){
    //                 $data['total_earnings_last_month'] = $this->Super_admin_model->my_total_earning('last');
    //             }
    //             else{
    //                 $data['total_earnings_last_month'] = '0';
    //             }
    //             if(!empty($this->Super_admin_model->my_total_upcoming_events())){
    //                 $data['total_upcoming_events'] = $this->Super_admin_model->my_total_upcoming_events();
    //             }
    //             else{
    //                 $data['total_upcoming_events'] = '0';
    //             }

    //             if(!empty($this->Super_admin_model->my_total_booked_properties())){
    //                 $data['total_booked_properties'] = $this->Super_admin_model->my_total_booked_properties();
    //             }
    //             else{
    //                 $data['total_booked_properties'] = '0';
    //             }

    //             if(!empty($this->Super_admin_model->my_total_booked_properties())){
    //                 $data['total_cancelled_properties'] = $this->Super_admin_model->my_total_booked_properties('cancelled');
    //             }
    //             else{
    //                 $data['total_cancelled_properties'] = '0';
    //             }

    //             if(!empty($this->Super_admin_model->my_total_booked_properties())){
    //                 $data['total_pending_properties'] = $this->Super_admin_model->my_total_booked_properties('pending');
    //             }
    //             else{
    //                 $data['total_pending_properties'] = '0';
    //             }
    //             //$data['upcoming_events'] = $this->Super_admin_model->get_all_upcoming_events_of_hosts_properties();
    //                 $this->load->view('dashboard/admin/dashboard3',$data);
    //             }else{ //admin

    //                 redirect('auth/select_role', 'refresh');
    //             }
                
    //             $this->load->view('dashboard/footer');
    //         }else{
    //             redirect('auth/select_role', 'refresh');
    //         }
    // }


    public function switch_to_user(){
        if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
        $this->data['title'] = 'Switch User';
          
           if (!$this->ion_auth->logged_in())
           {
               // redirect them to the login page
               redirect('auth/login', 'refresh');
           }
           else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
           {
               // redirect them to the home page because they must be an administrator to view this
               return show_error('You must be an administrator to view this page.');
           }
           else
           {
               // set the flash data error message if there is one
               $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
   
               //list the users
               $this->data['users'] = $this->ion_auth->users()->result();
               foreach ($this->data['users'] as $k => $user)
               {
                   $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
               }
               $this->load->view('dashboard/header',$this->data);
               $this->load->view('dashboard/admin/switch_to_user');
               $this->load->view('dashboard/footer');
            //   $this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
            
           }


   }   
          public function email_subscription(){
         if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
            $data['title'] = 'Subscription Emails List';
            if($this->session->has_userdata('group_id')){
                $this->load->view('dashboard/header', $data);
                if($this->session->userdata['group_id'] == 1){ //host
                    $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
                    $data['email_subscription'] = $this->Super_admin_model->email_subscription();
                    $this->load->view('dashboard/admin/email_subscription',$data);
                }else{ //admin

                    redirect('auth/select_role', 'refresh');
                }
                
                $this->load->view('dashboard/footer');
            }else{
                redirect('auth/select_role', 'refresh');
            }
    }
    public function property_recommend(){
         if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
            $data['title'] = 'All Properties';
            if($this->session->has_userdata('group_id')){
                $this->load->view('dashboard/header', $data);
                if($this->session->userdata['group_id'] == 1){ //host
                    $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
                     $data['propert_recommend'] = $this->Super_admin_model->get_properties();
                    $this->load->view('dashboard/admin/property_recommend',$data);
                }else{ //admin

                    redirect('auth/select_role', 'refresh');
                }
                
                $this->load->view('dashboard/footer');
            }else{
                redirect('auth/select_role', 'refresh');
            }
    }
    public function  update_recommend_properties(){
        if ($this->ion_auth->logged_in()){
            $id = $this->input->post('id');
            $approve = $this->input->post('approve');
            $field = $this->input->post('field');
            if($this->Super_admin_model->update_recommend_properties($id,$approve,$field)){
                echo "success";
            }else{
                echo "error";
            }   
        }else{
            echo "unauthorized";
        }
    }
}



        
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Property extends CI_Controller {
    public function __construct()
	{
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->model('Property_model');
        $this->load->model('Super_admin_model');
        
        // if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
        
        //$this->ion_auth->is_profile_complete();

    }
    
   	public function index()
	{
        $data['title'] = 'Property';

        $this->load->view('dashboard/header', $data);
          
        $this->load->view('property/index');
      
       $this->load->view('dashboard/footer');
    }

    

    public function get_host_bookings(){
        return $this->Property_model->get_host_bookings();
    }


    public function add()
	{
        
        if(isset($_REQUEST['submit'])){
           
            $property_data['property_title'] = $this->input->post('property_title');
            //$property_data['property_description'] = $this->input->post('property_description');
            $property_data['property_description_new'] = $this->input->post('property_description');
            $property_data['property_address'] = $this->input->post('property_address');
            $property_data['property_street_address'] = $this->input->post('property_street_address');
            $property_data['property_city'] = $this->input->post('property_city');
            $property_data['property_state'] = $this->input->post('property_state');
            $property_data['property_zip_code'] = $this->input->post('property_zip_code');
            $property_data['property_suburb'] = $this->input->post('property_suburb');
            $property_data['property_country'] = $this->input->post('property_country');
            $property_data['latitude'] = $this->input->post('latitude');
            $property_data['longitude'] = $this->input->post('longitude');
            $property_data['property_home_phone'] = $this->input->post('property_home_phone');
            $property_data['property_work_phone'] = $this->input->post('property_work_phone');
            $result_upload = 'empty';
            
            
            $last_insert_id = $this->Property_model->add_property($property_data);
            
            // if($_FILES['properties_images']['name'][0] !== ''){
                
            //     $result_upload = $this->do_upload_property_images($last_insert_id);
            // }

            // $data['result_upload'] = $result_upload;
            $data['last_insert_id'] = $last_insert_id;
           
            
            
            if($last_insert_id !== ''){
                $this->session->set_flashdata('flsh_msg', 'Property Added Successfully.');
                //$this->session->set_flashdata('alert', 'Add Images and events to properties.');
                $this->session->set_flashdata('tab_pane', 'images');
                redirect('property/edit/'.$last_insert_id,'refresh');
            }
        }

        $data['title'] = 'Add New Property';
        
        $data['country_names'] = $this->ion_auth->get_array_of_country();
        $this->load->view('dashboard/header', $data);
          
        $this->load->view('property/add');
      
       $this->load->view('dashboard/footer');

      
    }

    public function edit()
	{
        $segment_property_id = $this->uri->segment(3);
        
        
        if(isset($_REQUEST['submit'])){
           
            $property_data['property_title'] = $this->input->post('property_title');
           // $property_data['property_description'] = $this->input->post('property_description');
            $property_data['property_description_new'] = $this->input->post('property_description');
            $property_data['property_address'] = $this->input->post('property_address');
            $property_data['property_street_address'] = $this->input->post('property_street_address');
            $property_data['property_city'] = $this->input->post('property_city');
            $property_data['property_state'] = $this->input->post('property_state');
            $property_data['property_zip_code'] = $this->input->post('property_zip_code');
            $property_data['property_suburb'] = $this->input->post('property_suburb');
            $property_data['property_country'] = $this->input->post('property_country');
            $property_data['latitude'] = $this->input->post('latitude');
            $property_data['longitude'] = $this->input->post('longitude');
            $property_data['property_home_phone'] = $this->input->post('property_home_phone');
            $property_data['property_work_phone'] = $this->input->post('property_work_phone');
            $result_upload = 'empty';
 
            $resilt = $this->Property_model->edit_property($property_data,$segment_property_id);

             
            // if($_FILES['properties_images']['name'][0] !== ''){
                
            //     $result_upload = $this->do_upload_property_images($segment_property_id);
            // }

            // $data['result_upload'] = $result_upload;
            
            $this->session->set_flashdata('flsh_msg', 'Property Updated Successfully.');
            redirect('property/edit/'.$segment_property_id,'refresh');
            
        }

        $data['property_details'] = $this->get_property_details($segment_property_id);
        if($data['property_details']['property_details'] == ''){
            //redirect('404','refresh');
            redirect('property/listings','refresh');
        }
        $data['title'] = 'Edit Property Details';
        $data['country_names'] = $this->ion_auth->get_array_of_country();

        $data['amenities_fields'] = $this->Property_model->get_property_amenities_fields();
        $data['amenities_fields_data_of_this_property'] = $this->Property_model->get_property_amenities_fields_of_property($segment_property_id);

        $this->load->view('dashboard/header', $data);
          
        $this->load->view('property/edit');
      
       $this->load->view('dashboard/footer');

      
    }



    public function add_default_price()
	{
        $segment_property_id = $this->uri->segment(3);

        if(isset($_REQUEST['submit'])){
           
            $property_data['property_default_price'] = round($this->input->post('property_default_price') * 110 / 100, 2);
            $property_data['property_actual_price'] = $this->input->post('property_default_price');
         
 
            $resilt = $this->Property_model->edit_property($property_data,$segment_property_id);

            
            
            $this->session->set_flashdata('flsh_msg', 'Property Price Updated Successfully.');

            $this->session->set_flashdata('tab_pane', 'pricing');


            redirect('property/edit/'.$segment_property_id,'refresh');
            
        }

      
    }


    public function edit_images()
	{
        $segment_property_id = $this->uri->segment(3);

        if(isset($_REQUEST['submit'])){
           
            $result_upload = 'empty';
  
            if($_FILES['properties_images']['name'][0] !== ''){
                
                $result_upload = $this->do_upload_property_images($segment_property_id);
            }

            $data['result_upload'] = $result_upload;
            $this->session->set_flashdata('tab_pane', 'images');
            if($result_upload['error_flag']=='success'){
                $this->session->set_flashdata('flsh_msg', 'Images Uploaded Successfully.');    
            }else{
                $this->session->set_flashdata('flsh_msg', 'An error occured.');
                
            }
            
            
            redirect('property/edit/'.$segment_property_id,'refresh');
        }
    }


    public function edit_cover_photo()
	{
        $segment_property_id = $this->uri->segment(3);

        if(isset($_REQUEST['submit'])){
           
            $result_upload = 'empty';
  
            if($_FILES['property_cover_photo']['name'] !== ''){
                
                $result_upload = $this->do_upload_property_cover($segment_property_id);
            }

            $data['result_upload'] = $result_upload;
            
            $this->session->set_flashdata('flsh_msg', 'Cover photo uploaded.');
            $this->session->set_flashdata('tab_pane', 'images');
            redirect('property/edit/'.$segment_property_id,'refresh');
        }
    }


    
  
    public function do_upload_property_images($last_insert_id)
        {
            $rsult = array();
            $data_for_db = array();
            $error_flag = '';
            $filesCount = count($_FILES['properties_images']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['file']['name']     = $_FILES['properties_images']['name'][$i];
                $_FILES['file']['type']     = $_FILES['properties_images']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['properties_images']['tmp_name'][$i];
                $_FILES['file']['error']     = $_FILES['properties_images']['error'][$i];
                $_FILES['file']['size']     = $_FILES['properties_images']['size'][$i];

                if( ! is_dir('./uploads/properties/'.$last_insert_id) ){mkdir('./uploads/properties/'.$last_insert_id,0777,TRUE); };   
                
                $config['upload_path']          = './uploads/properties/'.$last_insert_id.'/';
                $config['allowed_types']        = 'jpg|jpeg|png|JPG|JPEG|PNG';
                // $config['max_size']             = 100;
                // $config['max_width']            = 1024;
                // $config['max_height']           = 768;
                $config['encrypt_name'] = TRUE;
                

                $this->load->library('upload', $config);

                

                if ( ! $this->upload->do_upload('file'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        $rsult[] = array('status'=>'error','data'=>$error);
                        $error_flag = 'error';
                }
                else
                {
                    if($error_flag !== 'error'){
                        $error_flag = 'success';
                    }
                        $data = array('upload_data' => $this->upload->data());
                        $rsult[] = array('status'=>'success','data'=>$data);
                        $data_for_db[] = array(
                            'property_id' => $last_insert_id,
                            'image_name' => $data['upload_data']['orig_name'],
                            'image_link' => $data['upload_data']['file_name'],
                            'created_by' => $this->ion_auth->user()->row()->id,
                            'modified_by'=> $this->ion_auth->user()->row()->id
                        );
                }

            }
            $rsult['error_flag'] = $error_flag;
            if($error_flag == 'success'){
                
                $this->db->insert_batch('properties_images', $data_for_db); 
            }
            
            return $rsult;
        }


    public function do_upload_property_cover($last_insert_id)
        {
            $rsult = array();
            $data_for_db = array();
            
                if( ! is_dir('./uploads/properties/'.$last_insert_id) ){mkdir('./uploads/properties/'.$last_insert_id,0777,TRUE); };   
                
                $config['upload_path']          = './uploads/properties/'.$last_insert_id.'/';
                $config['allowed_types']        = 'jpg|jpeg|png|JPG|JPEG|PNG';
                $config['encrypt_name'] = TRUE;
                

                $this->load->library('upload', $config);

                

                if ( ! $this->upload->do_upload('property_cover_photo'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        $rsult[] = array('status'=>'error','data'=>$error);
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        
                        $property_data = array(
                            
                            'property_cover_photo_is_approved' => '0',
                            'property_cover_photo' => $data['upload_data']['file_name'],
                            'modified_by'=> $this->ion_auth->user()->row()->id,
                            'modified_by_admin'=> $this->ion_auth->admin_id()
                        );

                        $this->db->where('property_id', $last_insert_id);
                        $this->db->update('properties_details', $property_data);

                        $rsult = array('status'=>'success','data'=>$data);
                }

            
            
            return $rsult;
        }

        public function do_upload_event_cover($last_insert_id)
        {
            $rsult = array();
            $data_for_db = array();
            
                if( ! is_dir('./uploads/properties/'.$last_insert_id) ){mkdir('./uploads/properties/'.$last_insert_id,0777,TRUE); };   
                
                $config['upload_path']          = './uploads/properties/'.$last_insert_id.'/';
                $config['allowed_types']        = 'jpg|jpeg|png|JPG|JPEG|PNG';
                $config['encrypt_name'] = TRUE;
                

                $this->load->library('upload', $config);

                

                if ( ! $this->upload->do_upload('event_cover_image'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        $rsult[] = array('status'=>'error','data'=>$error);
                }
                else
                {
                         $data = array('upload_data' => $this->upload->data());
                        
                        // $property_data = array(
                        //     'event_cover_image' => $data['upload_data']['file_name'];
                        // );

                        // $this->db->where('property_id', $last_insert_id);
                        // $this->db->update('properties_detailsproperty_details', $property_data);

                        $rsult = array('status'=>'success','event_cover_image'=>$data['upload_data']['file_name']);
                }

            
            
            return $rsult;
        }


        


        public function add_new_price(){
            $segment_property_id = $this->uri->segment(3);

            if(isset($_REQUEST['submit']) || isset($_REQUEST['submit_and_add'])){
                
               

                $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
                $event_id = $this->input->post('event_id');
                $to_date = $this->input->post('to_date');
                $from_date = $this->input->post('from_date');
                $price = round($this->input->post('price') * 110 / 100, 2);
                $actual_price = $this->input->post('price');

               
    
                if($this->Property_model->add_new_price($price,$actual_price,$event_id,$from_date,$to_date,$segment_property_id,$current_logged_in_user_id)){
                    $this->session->set_flashdata('flsh_msg', 'Price Added.');
                }else{
                    $this->session->set_flashdata('flsh_msg', 'Error while adding price.');
                }

                if(isset($_REQUEST['submit_and_add'])){
                    $this->session->set_flashdata('save_and_add_new', 'add_new_price_modal');
                }

                
                $this->session->set_flashdata('tab_pane', 'pricing');
                redirect('property/edit/'.$segment_property_id,'refresh');
            }
        }

    public function add_new_event()
	{
        $segment_property_id = $this->uri->segment(3);

        if(isset($_REQUEST['submit']) || isset($_REQUEST['submit_and_add'])){
            
            $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
            $event_name = $this->input->post('event_name');
            $to_date = $this->input->post('to_date');
            $from_date = $this->input->post('from_date');
            $event_description = $this->input->post('event_description');

            $event_cover_image = '';
            if($_FILES['event_cover_image']['name'] !== ''){
                
                $result_upload = $this->do_upload_event_cover($segment_property_id);
                if($result_upload['status']=='success'){
                    $event_cover_image = $result_upload['event_cover_image'];
                }
            }
            

            if($this->Property_model->add_new_event($event_name,$from_date,$to_date,$event_description,$event_cover_image,$segment_property_id,$current_logged_in_user_id)){
                $this->session->set_flashdata('flsh_msg', 'Event Added.');
            }else{
                $this->session->set_flashdata('flsh_msg', 'Error while adding event.');
            }

            if(isset($_REQUEST['submit_and_add'])){
                $this->session->set_flashdata('save_and_add_new', 'exampleModal');
            }
            
            $this->session->set_flashdata('tab_pane', 'events');
            redirect('property/edit/'.$segment_property_id,'refresh');
        }
    }


    public function edit_availability(){
        $segment_property_id = $this->uri->segment(3);
        $segment_availability_id = $this->uri->segment(4);

        if(isset($_REQUEST['submit']) || isset($_REQUEST['submit_and_add'])){
            
            $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
            
            $from_date = $this->input->post('from_date');
            $to_date = $this->input->post('to_date');
            $description = $this->input->post('description');
           


            if($this->Property_model->edit_availability($from_date,$to_date,$segment_availability_id,$current_logged_in_user_id,$description)){
                $this->session->set_flashdata('flsh_msg', 'Availability Updated successfully.');
            }else{
                $this->session->set_flashdata('flsh_msg', 'Error while updating Availability.');
            }

            if(isset($_REQUEST['submit_and_add'])){
                $this->session->set_flashdata('save_and_add_new', 'add_new_availability');
            }
            
            $this->session->set_flashdata('tab_pane', 'availability');
            redirect('property/edit/'.$segment_property_id,'refresh');
        }
    }

    public function edit_price(){
        $segment_property_id = $this->uri->segment(3);
        $segment_price_id = $this->uri->segment(4);

        if(isset($_REQUEST['submit']) || isset($_REQUEST['submit_and_add'])){
            
            $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
            $event_id = $this->input->post('event_id');
            $from_date = $this->input->post('from_date');
            $to_date = $this->input->post('to_date');
            $price = round($this->input->post('price') * 110 / 100, 2);
            $actual_price =  $this->input->post('price');


            if($this->Property_model->edit_price($event_id,$from_date,$to_date,$segment_price_id,$segment_property_id,$current_logged_in_user_id,$price,
                $actual_price)){
                $this->session->set_flashdata('flsh_msg', 'Price Updated successfully.');
            }else{
                $this->session->set_flashdata('flsh_msg', 'Error while updating price.');
            }

            if(isset($_REQUEST['submit_and_add'])){
                $this->session->set_flashdata('save_and_add_new', 'add_new_price_modal');
            }
            
            $this->session->set_flashdata('tab_pane', 'pricing');
            redirect('property/edit/'.$segment_property_id,'refresh');
        }
    }



    public function edit_event(){
        $segment_property_id = $this->uri->segment(3);
        $segment_event_id = $this->uri->segment(4);

        if(isset($_REQUEST['submit']) || isset($_REQUEST['submit_and_add'])){
            
            $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
            $event_name = $this->input->post('event_name');
            $from_date = $this->input->post('from_date');
            $to_date = $this->input->post('to_date');
            $event_description = $this->input->post('event_description');
            $result_upload['status']='error';
            $event_cover_image = '';
            if($_FILES['event_cover_image']['name'] !== ''){
                
                $result_upload = $this->do_upload_event_cover($segment_property_id);
                if($result_upload['status']=='success'){
                    $event_cover_image = $result_upload['event_cover_image'];
                }
            }
            


            if($this->Property_model->edit_event($event_name,$from_date,$to_date,$event_description,$event_cover_image,$segment_event_id,$current_logged_in_user_id)){
                $this->session->set_flashdata('flsh_msg', 'Event Update.');
            }else{
                $this->session->set_flashdata('flsh_msg', 'Error while updating event.');
            }

            if(isset($_REQUEST['submit_and_add'])){
                $this->session->set_flashdata('save_and_add_new', 'exampleModal');
            }
            
            $this->session->set_flashdata('tab_pane', 'events');
            redirect('property/edit/'.$segment_property_id,'refresh');
        }
    }


    public function add_new_availability(){
        $segment_property_id = $this->uri->segment(3);

        if(isset($_REQUEST['submit']) || isset($_REQUEST['submit_and_add'])){
            
            $availability_data['created_by'] = $this->ion_auth->user()->row()->id;
            $availability_data['modified_by'] = $this->ion_auth->user()->row()->id;
            $availability_data['from_date'] = $this->input->post('from_date');
            $availability_data['to_date'] = $this->input->post('to_date');
            $availability_data['description'] = $this->input->post('description');
            $availability_data['contact'] = $this->input->post('contact');
            $availability_data['property_id'] = $segment_property_id;
            $availability_data['modified_by_admin'] = $this->ion_auth->admin_id();

            if($this->Property_model->add_new_availability($availability_data)){
                $this->session->set_flashdata('flsh_msg', 'New Availability Added.');
            }else{
                $this->session->set_flashdata('flsh_msg', 'Error while adding Availability.');
            }

            if(isset($_REQUEST['submit_and_add'])){
                $this->session->set_flashdata('save_and_add_new', 'add_new_availability');
            }
            
            $this->session->set_flashdata('tab_pane', 'availability');
            
            redirect('property/edit/'.$segment_property_id,'refresh');
        }
    }



        public function listings(){
            if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
            $data['title'] = 'My Property Listings';
            //$this->ion_auth->is_profile_complete();
            
            if($this->session->has_userdata('group_id')){
                $this->load->view('dashboard/header', $data);
                if($this->session->userdata['group_id'] == 2){ //host
                    $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
                    $data['my_listings'] = $this->Property_model->get_users_listings($current_logged_in_user_id);
                    $this->load->view('property/my_listings',$data);
                }else{ //admin

                    redirect('auth/select_role', 'refresh');
                }
                
                $this->load->view('dashboard/footer');
            }else{
                redirect('auth/select_role', 'refresh');
            }
        }


    public function get_property_details($property_id){
       $property_details = array();

       $property_details['property_details'] = $this->Property_model->get_property_details($property_id);
       $property_details['property_images']  = $this->Property_model->get_property_images($property_id);
       $property_details['property_events']  = $this->Property_model->get_property_events($property_id);
       $property_details['properties_availability']  = $this->Property_model->get_properties_availability($property_id);
    //    $property_details['properties_booked_dates']  = $this->Property_model->get_properties_booked_dates($property_id);
       
       $property_details['opt_amenities_list']  = $this->Property_model->get_opt_amenities_list($property_id);
       $property_details['opt_amenities_others_list']  = $this->Property_model->get_opt_amenities_others_list($property_id);
       $dates_inner_array_str = '';
       if($property_details['properties_availability'] !== false){
        foreach($property_details['properties_availability'] as $avails){

            $period = new DatePeriod(
                new DateTime($avails->from_date),
                new DateInterval('P1D'),
                new DateTime(date('Y-m-d', strtotime('+1 day', strtotime($avails->to_date))))
           );

           foreach ($period as $key => $value) {
              if($dates_inner_array_str == ''){
                $dates_inner_array_str = '"'.$value->format('d F, Y').'"';
              }else{
                $dates_inner_array_str .= ',"'.$value->format('d F, Y').'"';
              }
            // $dates_inner_array_str[] = $value->format('Y-m-d');
        }
                
                
        }
       }
       $property_details['properties_availability_dates'] = '['.$dates_inner_array_str.']';  //'["2018/06/23","2018/06/25"]';


    //    if($property_details['properties_booked_dates'] !== false){
    //     foreach($property_details['properties_booked_dates'] as $avails){

    //         $period = new DatePeriod(
    //             new DateTime($avails->date),
    //             new DateInterval('P1D'),
    //             new DateTime(date('Y-m-d', strtotime('+1 day', strtotime($avails->date))))
    //        );

    //        foreach ($period as $key => $value) {
    //           if($dates_inner_array_str == ''){
    //             $dates_inner_array_str = '"'.$value->format('d F, Y').'"';
    //           }else{
    //             $dates_inner_array_str .= ',"'.$value->format('d F, Y').'"';
    //           }
    //         // $dates_inner_array_str[] = $value->format('Y-m-d');
    //     }
                
                
    //     }
    //    }
    //    $property_details['properties_disabled_dates'] = '['.$dates_inner_array_str.']';

       $property_details['properties_pricing'] = $this->Property_model->get_property_pricing($property_id);
       $dates_inner_array_strperiod_price_dates = '';
       if($property_details['properties_pricing'] !== false){
        foreach($property_details['properties_pricing'] as $avails_price_dates){

            $period_price_dates = new DatePeriod(
                new DateTime($avails_price_dates['from_date']),
                new DateInterval('P1D'),
                new DateTime(date('Y-m-d', strtotime('+1 day', strtotime($avails_price_dates['to_date']))))
           );

           foreach ($period_price_dates as $keyperiod_price_dates => $valueperiod_price_dates) {
              if($dates_inner_array_strperiod_price_dates == ''){
                $dates_inner_array_strperiod_price_dates = '"'.$valueperiod_price_dates->format('d F, Y').'"';
              }else{
                $dates_inner_array_strperiod_price_dates .= ',"'.$valueperiod_price_dates->format('d F, Y').'"';
              }
            // $dates_inner_array_str[] = $value->format('Y-m-d');
        }
                
                
        }
       }
       $property_details['properties_availability_dates_for_pricing'] = '['.$dates_inner_array_strperiod_price_dates.']';  //'["2018/06/23","2018/06/25"]';


       $property_details['properties_events_list'] = $this->Property_model->get_events_of_property($property_id);
       
       return $property_details;
    }


    public function loadData()
	{
		$loadType=$_REQUEST['loadType'];
		$loadId=$_REQUEST['loadId'];

		
		$result=$this->Property_model->getData($loadType,$loadId);
		$HTML="";
		
		if($result->num_rows() > 0){
			foreach($result->result() as $list){
				$HTML.="<option datavalue='".$list->id."' value='".$list->id."'>".$list->name."</option>";
			}
		}
		echo $HTML;
	}
    
    
public function loadData_foredit()
{
    $loadType=$_REQUEST['loadType'];
    $loadId=$_REQUEST['loadId'];
            $selected_stt=$_REQUEST['selected_stt'];
             $selected_ct=$_REQUEST['selected_ct'];
            

    
    $result=$this->Property_model->getData($loadType,$loadId);
    $HTML="";
    
    if($result->num_rows() > 0){
        foreach($result->result() as $list){
                        if($list->id == $selected_stt){
                          $HTML.="<option selected='selected' datavalue='".$list->id."' value='".$list->id."'>".$list->name."</option>";  
                             echo "<script>selectCity_foreditcity(".$list->id.",'".$selected_ct."')</script>";
                               //echo "<script>$('select.city').find('option[value=Hofn]').attr('selected','selected');</script>";
                        }else{
                            $HTML.="<option  datavalue='".$list->id."' value='".$list->id."'>".$list->name."</option>";
                        }
            
        }
    }
    echo $HTML;
}
    
    
public function loadData_foreditct()
{
    $loadType=$_REQUEST['loadType'];
    $loadId=$_REQUEST['loadId'];
            $selected_ct=$_REQUEST['selected_ct'];
            

    
    $result=$this->Property_model->getData($loadType,$loadId);
    $HTML="";
    
    if($result->num_rows() > 0){
        foreach($result->result() as $list){
                        if($list->id == $selected_ct){
                          $HTML.="<option selected='selected' datavalue='".$list->id."' value='".$list->id."'>".$list->name."</option>";  
                             
                        }else{
                            $HTML.="<option  datavalue='".$list->id."' value='".$list->id."'>".$list->name."</option>";
                        }
            
        }
    }
    echo $HTML;
}     


    public function ajax_delete_property_image(){
        if ($this->ion_auth->logged_in() && isset($_REQUEST['id'])){
            if($this->Property_model->delete_property_image($_REQUEST['id'])){
                echo "success";
            }else{
                echo "error";
            }
        }else{
            echo "unauthorized";
        }
        
    }


    public function ajax_delete_event(){
        if ($this->ion_auth->logged_in() && isset($_REQUEST['id'])){
            if($this->Property_model->delete_event($_REQUEST['id'])){
                echo "success";
            }else{
                echo "error";
            }
        }else{
            echo "unauthorized";
        }
        
    }

    public function ajax_delete_price(){
        if ($this->ion_auth->logged_in() && isset($_REQUEST['id'])){
            if($this->Property_model->delete_price($_REQUEST['id'])){
                echo "success";
            }else{
                echo "error";
            }
        }else{
            echo "unauthorized";
        }
        
    }

    public function ajax_toggle_active_this_event(){
        if ($this->ion_auth->logged_in() && isset($_REQUEST['id']) && isset($_REQUEST['is_active'])){
            if($this->Property_model->toggle_active_this_event($_REQUEST['id'],$_REQUEST['is_active'])){
                echo "success";
            }else{
                echo "error";
            }
        }else{
            echo "unauthorized";
        }
    }

    public function ajax_toggle_active_this_price(){
        if ($this->ion_auth->logged_in() && isset($_REQUEST['id']) && isset($_REQUEST['is_active'])){
            if($this->Property_model->toggle_active_this_price($_REQUEST['id'],$_REQUEST['is_active'])){
                echo "success";
            }else{
                echo "error";
            }
        }else{
            echo "unauthorized";
        }
    }

    public function ajax_toggle_active_this_property(){
        if ($this->ion_auth->logged_in() && isset($_REQUEST['id']) && isset($_REQUEST['is_active'])){
            if($this->Property_model->toggle_active_this_property($_REQUEST['id'],$_REQUEST['is_active'])){
                echo "success";
            }else{
                echo "error";
            }
        }else{
            echo "unauthorized";
        }
    }

    
    public function ajax_remove_this_availabilities(){
        if ($this->ion_auth->logged_in() && isset($_REQUEST['id'])){
            if($this->Property_model->delete_availability($_REQUEST['id'])){
                echo "success";
            }else{
                echo "error";
            }
        }else{
            echo "unauthorized";
        }
        
    }

    public function ajax_add_remove_amenity_from_property(){
        
        if ($this->ion_auth->logged_in() && isset($_REQUEST['property_id']) && isset($_REQUEST['amenities_id'])){
            if($this->Property_model->add_remove_amenity_from_property($_REQUEST['property_id'],$_REQUEST['amenities_id'],$_REQUEST['state'])){
                echo "Successfully Updated.";
            }else{
                echo "Error while update";
            }
            //return $this->Property_model->add_remove_amenity_from_property($_REQUEST['property_id'],$_REQUEST['amenities_id'],$_REQUEST['state']);
        }else{
            echo "Unauthorized Action.";
        }
    }

    public function ajax_add_remove_amenity_other_from_property(){
        
        if ($this->ion_auth->logged_in() && isset($_REQUEST['property_id']) && isset($_REQUEST['amenities_other_id'])){
            if($this->Property_model->add_remove_amenity_other_from_property($_REQUEST['property_id'],$_REQUEST['amenities_other_id'],$_REQUEST['state'])){
                echo "Successfully Updated.";
            }else{
                echo "Error while update";
            }
            //return $this->Property_model->add_remove_amenity_from_property($_REQUEST['property_id'],$_REQUEST['amenities_id'],$_REQUEST['state']);
        }else{
            echo "Unauthorized Action.";
        }
    }






    public function dashboard()
	{
        
        if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
        
        //$this->ion_auth->is_profile_complete();
        
        if($this->session->has_userdata('group_id')){
            
            if($this->session->userdata['group_id'] == 3){ //guest
                $data['title'] = 'Guest Dashboard';

                $data['my_bookings'] = $this->Property_model->get_guest_bookings();
                $data['total_spend_till_now'] = $this->Property_model->get_guest_total_spend_till_now();
                $data['total_spend_last_month'] = $this->Property_model->get_guest_total_spend_till_now('last');
                $data['total_spend_current_month'] = $this->Property_model->get_guest_total_spend_till_now('current');
                $data['total_bookings'] = $this->Property_model->get_guest_total_bookings();
                // $data['count_my_bookings'] =  $this->get_host_bookings() != false ? count($this->get_host_bookings()) : 0;//count($this->get_host_bookings());
                


                $this->load->view('dashboard/header', $data);
                $this->load->view('dashboard/guest/dashboard');
            }else if($this->session->userdata['group_id'] == 2){ //host
                $data['title'] = 'Host Dashboard';
                // $data['my_bookings'] = $this->get_host_bookings();
                // $data['count_my_bookings'] =  $this->get_host_bookings() != false ? count($this->get_host_bookings()) : 0;//count($this->get_host_bookings());
                // if(is_array($this->Property_model->get_users_listings($this->ion_auth->user()->row()->id))){
                //     $data['count_my_properties'] =$this->Property_model->get_users_listings($this->ion_auth->user()->row()->id) != false ? count($this->Property_model->get_users_listings($this->ion_auth->user()->row()->id)) : 0;
                // }else{
                //     $data['count_my_properties'] =  '0';
                // }
                // $data['count_my_guests'] = count($this->ion_auth->get_my_clients());
                
                if(!empty($this->Property_model->my_total_earning())){
                    $data['total_earnings_till_now'] = $this->Property_model->my_total_earning();
                }
                else{
                    $data['total_earnings_till_now'] = '0';
                }
                if(!empty($this->Property_model->my_total_earning('current'))){
                    $data['total_earnings_current_month'] = $this->Property_model->my_total_earning('current');
                }
                else{
                    $data['total_earnings_current_month'] = '0';
                }
                if(!empty($this->Property_model->my_total_earning('last'))){
                    $data['total_earnings_last_month'] = $this->Property_model->my_total_earning('last');
                }
                else{
                    $data['total_earnings_last_month'] = '0';
                }


                
                if(!empty($this->Property_model->my_total_upcoming_events())){
                    $data['total_upcoming_events'] = $this->Property_model->my_total_upcoming_events();
                }
                else{
                    $data['total_upcoming_events'] = '0';
                }

                if(!empty($this->Property_model->my_total_booked_properties())){
                    $data['total_booked_properties'] = $this->Property_model->my_total_booked_properties();
                }
                else{
                    $data['total_booked_properties'] = '0';
                }

                if(!empty($this->Property_model->my_total_booked_properties())){
                    $data['total_cancelled_properties'] = $this->Property_model->my_total_booked_properties('cancelled');
                }
                else{
                    $data['total_cancelled_properties'] = '0';
                }

                if(!empty($this->Property_model->my_total_booked_properties())){
                    $data['total_pending_properties'] = $this->Property_model->my_total_booked_properties('pending');
                }
                else{
                    $data['total_pending_properties'] = '0';
                }

                $data['upcoming_events'] = $this->Property_model->get_all_upcoming_events_of_hosts_properties();
                

                
                
                
                
                $this->load->view('dashboard/header', $data);
                $this->load->view('dashboard/host/dashboard');
            }else if($this->session->userdata['group_id'] == 1){ //admin
                $data['title'] = 'Admin Dashboard';

                if(!empty($this->Super_admin_model->my_total_earning())){
                    $data['total_earnings_till_now'] = $this->Super_admin_model->my_total_earning();
                }
                else{
                    $data['total_earnings_till_now'] = '0';
                }
                if(!empty($this->Super_admin_model->my_total_earning('current'))){
                    $data['total_earnings_current_month'] = $this->Super_admin_model->my_total_earning('current');
                }
                else{
                    $data['total_earnings_current_month'] = '0';
                }
                if(!empty($this->Super_admin_model->my_total_earning('last'))){
                    $data['total_earnings_last_month'] = $this->Super_admin_model->my_total_earning('last');
                }
                else{
                    $data['total_earnings_last_month'] = '0';
                }
                if(!empty($this->Super_admin_model->my_total_upcoming_events())){
                    $data['total_upcoming_events'] = $this->Super_admin_model->my_total_upcoming_events();
                }
                else{
                    $data['total_upcoming_events'] = '0';
                }

                if(!empty($this->Super_admin_model->my_total_booked_properties())){
                    $data['total_booked_properties'] = $this->Super_admin_model->my_total_booked_properties();
                }
                else{
                    $data['total_booked_properties'] = '0';
                }

                if(!empty($this->Super_admin_model->my_total_booked_properties())){
                    $data['total_cancelled_properties'] = $this->Super_admin_model->my_total_booked_properties('cancelled');
                }
                else{
                    $data['total_cancelled_properties'] = '0';
                }

                if(!empty($this->Super_admin_model->my_total_booked_properties())){
                    $data['total_pending_properties'] = $this->Super_admin_model->my_total_booked_properties('pending');
                }
                else{
                    $data['total_pending_properties'] = '0';
                }


                $this->load->view('dashboard/header', $data);
                $this->load->view('dashboard/admin/dashboard3');
            }
            
            $this->load->view('dashboard/footer');
        }else{
            redirect('auth/select_role', 'refresh');
        }
        
    }

    
    

    public function my_booking_list_page(){
        if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
        
        //$this->ion_auth->is_profile_complete();
        
        if($this->session->has_userdata('group_id')){
            
            if($this->session->userdata['group_id'] == 3){ //guest
                $data['title'] = 'Guest Dashboard';
                $this->load->view('dashboard/header', $data);
                $this->load->view('dashboard/guest/index');
            }else if($this->session->userdata['group_id'] == 2){ //host
                $data['title'] = 'Host Dashboard';
                $data['my_bookings'] = $this->get_host_bookings();
                $this->load->view('dashboard/header', $data);
                $this->load->view('dashboard/host/index');
            }else if($this->session->userdata['group_id'] == 1){ //admin
                $data['title'] = 'Admin Dashboard';
                $this->load->view('dashboard/header', $data);
                $this->load->view('dashboard/admin/index');
            }
            
            $this->load->view('dashboard/footer');
        }else{
            redirect('auth/select_role', 'refresh');
        }
    }
    

    public function booking(){
        
        if (null == $this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('flsh_msg', 'Please select a valid Booking.');
            redirect('bookings', 'refresh',array());
        }

        $data['title'] = 'Booking #'.$this->uri->segment(3);

        $data['booking'] = $this->Property_model->get_host_booking_by_id($this->uri->segment(3));
        if(!$data['booking']){
            $this->session->set_flashdata('flsh_msg', 'Booking ID not exist.');
            redirect('bookings', 'refresh');
        }

        $this->load->view('dashboard/header', $data);
          
        $this->load->view('property/booking');
      
       $this->load->view('dashboard/footer');
    }

    public function booking_guest(){
        
        if (null == $this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('flsh_msg', 'Please select a valid Booking.');
            redirect('bookings', 'refresh',array());
        }

        $data['title'] = 'Booking #'.$this->uri->segment(3);
        $data['booking'] = $this->Property_model->get_guest_booking_by_id($this->uri->segment(3));
        if(!$data['booking']){
            $this->session->set_flashdata('flsh_msg', 'Booking ID not exist.');
            redirect('dashboard', 'refresh');
        }

        $this->load->view('dashboard/header', $data);
          
        $this->load->view('dashboard/guest/booking');
      
       $this->load->view('dashboard/footer');
    }


    public function add_edit_amenity_fields(){
        if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
        if (null == $this->uri->segment(3) || !is_numeric($this->uri->segment(3))){
            $this->session->set_flashdata('flsh_msg', 'Something went wrong.');
            redirect('property/listings', 'refresh');
        }
        $property_id = $this->uri->segment(3);
        
        if(!isset($_REQUEST['amenity_data']) || !isset($_REQUEST['amenity_data']['amenities_fields']) || !isset($_REQUEST['amenity_data']['field_data'])){
            $this->session->set_flashdata('tab_pane', 'amenities');
            $this->session->set_flashdata('flsh_msg', 'Please select a valid input.');
            redirect('property/edit/'.$property_id, 'refresh');
        }else{
            // echo "<pre>"; print_r($_REQUEST);die('sadd');
            $amenity_child_data = array();
            foreach($_REQUEST['amenity_data']['amenities_fields'] as $amsKEY=>$amsVAL){
                if($amsVAL == 'Select Amenity'){

                }else{
                    $amenity_child_data[$amsKEY]['property_id'] = $property_id;
                    $amenity_child_data[$amsKEY]['amenities_fields_id'] = $this->Property_model->get_amenities_fields_id_from_child_field_id($_REQUEST['amenity_data']['amenities_fields'][$amsKEY]);
                    $amenity_child_data[$amsKEY]['amenities_fields_childs_id'] = $_REQUEST['amenity_data']['amenities_fields'][$amsKEY];
                    $amenity_child_data[$amsKEY]['field_data'] = $_REQUEST['amenity_data']['field_data'][$amsKEY];
                    $amenity_child_data[$amsKEY]['created_by'] = $this->ion_auth->user()->row()->id;
                    $amenity_child_data[$amsKEY]['modified_by'] = $this->ion_auth->user()->row()->id;
                    $amenity_child_data[$amsKEY]['modified_by_admin'] = $this->ion_auth->admin_id();
                }
               
            }
            
if(empty($amenity_child_data)){
    if($this->Property_model->make_old_properties_amenties_fields($property_id)){
        $this->session->set_flashdata('flsh_msg', 'Updated successfully.');
    }else{
        $this->session->set_flashdata('flsh_msg', 'Error in removing Amenities.');
    }
    $this->session->set_flashdata('tab_pane', 'amenities');
    
    redirect('property/edit/'.$property_id, 'refresh');
}else{
            if($this->Property_model->add_properties_amenties_fields($amenity_child_data,$property_id)){
                $this->session->set_flashdata('tab_pane', 'amenities');
                $this->session->set_flashdata('flsh_msg', 'Amenities saved successfully.');
                redirect('property/edit/'.$property_id, 'refresh');
            }else{
                $this->session->set_flashdata('tab_pane', 'amenities');
                $this->session->set_flashdata('flsh_msg', 'Eror while saving Amenities');
                redirect('property/edit/'.$property_id, 'refresh');
            }
        }   

            
        }
        
    }

/*payment history*/
  public function payment_history(){
            if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
            $data['title'] = 'Payment History';
            //$this->ion_auth->is_profile_complete();
            
            if($this->session->has_userdata('group_id')){
                $this->load->view('dashboard/header', $data);
                if($this->session->userdata['group_id'] == 3){ //host
                    $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
                    $data['guest_payment_history'] = $this->Property_model->get_guest_payment_history($current_logged_in_user_id);
                    $this->load->view('dashboard/guest/payment_history',$data);
                }else{ //admin

                    redirect('auth/select_role', 'refresh');
                }
                
                $this->load->view('dashboard/footer');
            }else{
                redirect('auth/select_role', 'refresh');
            }
        }
    /**end payment history */
    /*payment refund*/
      public function payment_refund_history(){
            if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
            $data['title'] = 'Payment Refund';
            //$this->ion_auth->is_profile_complete();
            
            if($this->session->has_userdata('group_id')){
                $this->load->view('dashboard/header', $data);
                if($this->session->userdata['group_id'] == 3){ //host
                    $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
                    $data['payment_refund_history'] = $this->Property_model->get_payment_refund_history($current_logged_in_user_id);
                    $this->load->view('dashboard/guest/payment_refund_history',$data);
                }else{ //admin

                    redirect('auth/select_role', 'refresh');
                }
                
                $this->load->view('dashboard/footer');
            }else{
                redirect('auth/select_role', 'refresh');
            }
        }
        /*invitation of single property*/
    public function property_invitation(){
        if(isset($_REQUEST['email_invit'])){
            $property_slug = $this->input->post('property_slug');
            //$segment_property_id = $this->uri->segment(3);
            $to = $this->input->post('email');
            $template_name = 'property_invite';
            $data_for_template =array();
            $data_for_template['property_title'] = $this->input->post('property_title');
            $data_for_template['property_country'] = $this->input->post('property_country');
            $data_for_template['property_city'] = $this->input->post('property_city');
            $data_for_template['property_state'] = $this->input->post('property_state');
            $data_for_template['property_rating'] = $this->input->post('property_rating');
            $data_for_template['property_link'] = base_url().'index.php/listing/'.$property_slug;

            
            $data_for_template['property_more_details'] = $this->input->post('property_more_details');
            
            $subject = 'You have invitation to a Mats property.';
            $message = 'Have a look at this Mats Property.';
            $this->ion_auth->send_mail($to,$template_name,$subject,$data_for_template,$message);
            $this->session->set_flashdata('flsh_msg_new', 'Invitation Sent Successfully.');
            redirect('/listing/'.$property_slug,'refresh');
            //redirect('/listing/'.$property_slug);
        }    
    }
   /*payment refund*/
      public function property_review(){
            if (!$this->ion_auth->logged_in()){redirect('auth/login', 'refresh');}
            $data['title'] = 'My Property Ratings';
            //$this->ion_auth->is_profile_complete();
            
            if($this->session->has_userdata('group_id')){
                $this->load->view('dashboard/header', $data);
                if($this->session->userdata['group_id'] == 2){ //host
                    $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
                    $data['property_review'] = $this->Property_model->property_review($current_logged_in_user_id);
                    $this->load->view('dashboard/host/property_review',$data);
                }else{ //admin

                    redirect('auth/select_role', 'refresh');
                }
                
                $this->load->view('dashboard/footer');
            }else{
                redirect('auth/select_role', 'refresh');
            }
        }
    public function get_subrub()
    {   
        if(isset($_REQUEST['zipcode'])){
        $zipcode = $_REQUEST['zipcode'];
        $result=$this->Property_model->get_subrub($zipcode);
    }
    }
    public function get_zipcode()
    {   
        if(isset($_REQUEST['suburb'])){
        $suburb = $_REQUEST['suburb'];
        $result=$this->Property_model->get_zipcode($suburb);
    }
    }
    // public function get_search_props_ajax(){
    //     $this->Property_model->get_search_props();
    // }

}



        
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('url', 'language'));
        $this->load->model('Cart_model');
        $this->load->model('Property_model');
        $this->load->library("braintree_lib");

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        if (!$this->ion_auth->logged_in())
		{
            die('unauthorized');
		}
        
    }

    public function add_to_cart(){
        $cart_data['property_id'] = $this->input->post('property_id');
        $cart_data['from_date'] = $this->ion_auth->change_date_format_with_english_month($this->input->post('from_date'));
        $cart_data['to_date'] = $this->ion_auth->change_date_format_with_english_month($this->input->post('to_date'));
        $notes = $this->input->post('notes');
        if($this->check_cart_session_if_property_with_same_dates_exist($cart_data) == true){
            echo "Property with same dates is already in your cart.";
        }else{
            if($this->Cart_model->add_to_wishlist_n_cart($cart_data,$notes)){
                echo "Property is added to trip for ".$this->input->post('from_date')." To ".$this->input->post('to_date');
            }else{
                echo "error";
            }
        }
        
    }

    public function checkout(){
        $this->data['title'] = 'Checkout';
        $this->data['country_names'] = $this->ion_auth->get_array_of_country();
        $this->data['recommended_props'] = $this->Property_model->get_recommended_props('4');
        $this->load->view('include/header',$this->data);
        $this->load->view('checkout');
        $this->load->view('include/footer');
      
    }

    public function payment(){
        $this->data['title'] = 'Payment';
        // $this->data['country_names'] = $this->ion_auth->get_array_of_country();
        // $this->data['recommended_props'] = $this->Property_model->get_recommended_props('4');
        $this->load->view('include/header',$this->data);
        $this->load->view('payment');
        $this->load->view('include/footer');
      
    }

    public function check_cart_session_if_property_with_same_dates_exist($cart_data){
        $existing_cart = array();
        $existing_cart = $this->session->userdata('cart');
        
        $check_property_id = false;
        $check_from_date = false;
        $check_to_date = false;
        $check = false; 

        if(is_array($existing_cart))
        {
            foreach($existing_cart as $existing_cart_item){
                if($check_property_id == true && $check_from_date == true && $check_to_date == true){
                    $check = true;
                    return true;
                }else
                {
                    if($existing_cart_item['property_id'] == $cart_data['property_id']){
                        $check_property_id = true;
                        if($existing_cart_item['from_date'] == $cart_data['from_date']){
                            $check_from_date = true;
                        }else{
                            $check_from_date = false;
                        }
                        if($existing_cart_item['to_date'] == $cart_data['to_date']){
                            $check_to_date = true;
                        }else{
                            $check_to_date = false;
                        }
                    }else{
                        $check_property_id = false;
                    }
                }
            }

            if($check_property_id == true && $check_from_date == true && $check_to_date == true){
                $check = true;
            }

        }else{
            $check = false;
        }
        
        return $check;
    }

    
    public function remove_this_cart_item(){
        $cartItemId = $this->input->post('cartItemId');
        if($this->Cart_model->remove_this_cart_item($cartItemId)){
            echo "success";
        }else{
            echo "error";
        }
    }

    public function get_property_prices_on_particular_dates_as_string(){
        $prize_criteria['property_id'] = $this->input->post('property_id');
       
        $date_from_date = date_create_from_format('j F, Y', $this->input->post('from_date'));
        $date_to_date = date_create_from_format('j F, Y', $this->input->post('to_date'));
       

        $prize_criteria['from_date'] = date_format($date_from_date, 'Y-m-d');
        $prize_criteria['to_date'] = date_format($date_to_date, 'Y-m-d');

        // $prize_criteria['from_date'] = $this->ion_auth->change_date_format($this->input->post('from_date'));
        // $prize_criteria['to_date'] = $this->ion_auth->change_date_format($this->input->post('to_date'));
        //echo "disabled_dates_selected";

        // echo "<pre>";print_r($prize_criteria);echo "</pre>";
        $prize = $this->Property_model->get_property_prices_on_particular_dates_as_string_with_status($prize_criteria);
        if($prize['status'] == 'success'){
            $msg = $prize['price'];
            echo "Total Amount to be charged $".$msg."USD";
        }else{
            $msg = $prize['msg'];
            echo $msg;
            echo "<script>calenderReset();</script>";
        }
        
    }

    


    public function cancel_booking_by_guest(){
        $reason_behind_cancel_booking = $this->input->post('reason_behind_cancel_booking');
        $notes_if_any = $this->input->post('notes_if_any');
        $booking_id = $this->uri->segment(3);

        $result = $this->Cart_model->cancel_booking_by_guest($booking_id,$reason_behind_cancel_booking,$notes_if_any);
        switch ($result) {
            case "success":
                $result = "Booking Canceled.";
                break;
            case "unauthorised":
            $result = "You are not authorised to perform this action.";
                break;
            case "error":
            $result = "An error occured.";
                break;
            default:
            $result = "Unknown error occured.";
        }


        $this->session->set_flashdata('flsh_msg', $result);
        redirect('dashboard','refresh');


    }

    public function cancel_booking_by_host(){
        $reason_behind_cancel_booking = $this->input->post('reason_behind_cancel_booking');
        $notes_if_any = $this->input->post('notes_if_any');
        $booking_id = $this->uri->segment(3);

        $result = $this->Cart_model->cancel_booking_by_host($booking_id,$reason_behind_cancel_booking,$notes_if_any);
        switch ($result) {
            case "success":
                $result = "Booking Canceled.";
                break;
            case "unauthorised":
            $result = "You are not authorised to perform this action.";
                break;
            case "error":
            $result = "An error occured.";
                break;
            default:
            $result = "Unknown error occured.";
        }


        $this->session->set_flashdata('flsh_msg', $result);
        redirect('bookings','refresh');


    }

   

    public function ajax_confirm_this_booking(){
        if ($this->ion_auth->logged_in() && isset($_REQUEST['booking_id'])){
            
            $result = $this->Cart_model->ajax_confirm_this_booking_by_host($_REQUEST['booking_id']);
               echo $result;
        }else{
            echo "unauthorized";
        }

    }
    /*update checkout*/
    public function update_add_to_cart(){
        
            $cart_data['wish_id'] = $this->input->post('wish_id');
            $cart_data['property_id'] = $this->input->post('property_id');
            $cart_data['from_date'] = $this->ion_auth->change_date_format_with_english_month($this->input->post('from_date'));
            $cart_data['to_date'] = $this->ion_auth->change_date_format_with_english_month($this->input->post('to_date'));
            $notes = $this->input->post('notes');
            $cart_data['notes'] = $notes;
            if($this->check_cart_update_session_if_property_with_same_dates_exist($cart_data) == true){
                echo "Property with same dates is already in your cart.";
            }
            else{
                unset($cart_data['notes']);
                if($this->Cart_model->update_add_to_wishlist_n_cart($cart_data,$notes)){
                    echo "Property is update to trip for ".$this->input->post('from_date')." To ".$this->input->post('to_date');
                }
                else{
                    echo "error";
                }
            }
        
    }

    public function check_cart_update_session_if_property_with_same_dates_exist($cart_data){
        $existing_cart = array();
        $existing_cart = $this->session->userdata('cart');
        
        $check_property_id = false;
        $check_from_date = false;
        $check_to_date = false;
        $check_notes = false;
        $check = false; 

        if(is_array($existing_cart))
        {
            foreach($existing_cart as $existing_cart_item){
                if($check_property_id == true && $check_from_date == true && $check_to_date == true && $check_notes == true){
                    $check = true;
                    return true;
                }else
                {
                    if($existing_cart_item['property_id'] == $cart_data['property_id']){
                        $check_property_id = true;
                        if($existing_cart_item['from_date'] == $cart_data['from_date']){
                            $check_from_date = true;
                        }else{
                            $check_from_date = false;
                        }
                        if($existing_cart_item['to_date'] == $cart_data['to_date']){
                            $check_to_date = true;
                        }else{
                            $check_to_date = false;
                        }
                        if($existing_cart_item['notes'] == $cart_data['notes']){
                            $check_notes = true;
                        }else{
                            $check_notes = false;
                        }
                    }else{
                        $check_property_id = false;
                    }
                }
            }

            if($check_property_id == true && $check_from_date == true && $check_to_date == true && $check_notes == true){
                $check = true;
            }

        }else{
            $check = false;
        }
        
        return $check;
    }
    private function printJSON($var){
                echo json_encode($var);
        }

    public function get_token()
    {
        $token = $this->braintree_lib->create_client_token();
        $this->printJSON($token);
    }
    public function card_success(){
       // $paypalInfo = $_POST;
         if(!isset($_POST['card_number'])){
             show_404();
         }
        if(isset($_POST['cart_payment'])){
        $price = $_POST['amount']; 
        $card_number=str_replace("+","",$_POST['card_number']);  
        $card_name=$_POST['card_number'];
        $expiry_month=$_POST['expiry_month'];
        $expiry_year=$_POST['expiry_year'];
        $cvv=$_POST['cvv'];
        $expirationDate=$expiry_month.'/'.$expiry_year;
      }
        $result = Braintree_Transaction::sale(array(
        'amount' => $price,
        'creditCard' => array(
        'number' => $card_number,
        'cardholderName' => $card_name,
        'expirationDate' => $expirationDate,
        'cvv' => $cvv
        )
        ));
        if ($result->success) 
        {
        if($result->transaction->id)
        {
            $braintreeCode=$result->transaction->id;
            $created_date =  $result->transaction->createdAt;
            foreach($created_date as $k=>$date2){
                if($k == 'date'){
                    $date =  $date2; 
                }
            }
            $data['payer_email'] = $result->transaction->creditCard['cardholderName'];
            $data['payer_id'] = $result->transaction->id;
            $data['payer_status'] = $result->transaction->status;
            $data['txn_id'] = $result->transaction->id;
            $data['mc_currency'] = $result->transaction->currencyIsoCode;
            $data['payment_gross'] = $result->transaction->amount;
            $data['payment_type'] = '';         
            $data['txn_type'] = '';
            $data['payment_date'] = $date;
            $data['business'] = '';
            $data['receiver_id'] = $result->transaction->id;
            $data['users_id'] = $_POST['custom'];
            $data['verify_sign'] = '';
            $data['type'] = 'braintree';
            $card_data['card_number'] = $result->transaction->creditCard['cardholderName'];
            $card_data['holder_name'] = $result->transaction->creditCard['cardholderName'];
            $card_data['expire_month'] = $result->transaction->creditCard['expirationMonth'];
            $card_data['expire_year'] = $result->transaction->creditCard['expirationYear'];
            $card_data['user_id'] = $_POST['custom'];
            $card_data['payment_date'] = $date;
            $card_data['txn_id'] = $result->transaction->id; 
            $insertTransaction1 = $this->Cart_model->insert_cards($card_data);
            $insertTransaction = $this->Cart_model->insertTransaction($data);
            if($insertTransaction == 'added' || $insertTransaction == 'already_exist'){
            $this->data['is_success'] = true;
            if($insertTransaction == 'added'){
                $this->data['status'] = 'Well Done! Payment Successfully Placed <div class="idsavetxt">Save Your TXN Id</div>';
            }else if($insertTransaction == 'already_exist'){
                $this->data['status'] = 'Payment Successfully Placed already <div class="idsavetxt">Save Your TXN Id</div>';
            }else{
                $this->data['status'] = 'Payment Successfully Placed. <div class="idsavetxt">Save Your TXN Id</div>';
            }
            $this->data['title'] = 'Payment Success';
            
            $this->data['status_details'] = 'Payment Successfully placed. ID - '.$result->transaction->id;
            if($this->ion_auth->logged_in()){
               // echo '{"OrderStatus": [{"status":"1"}]}';
                $this->load->view('include/header',$this->data);
                $this->load->view('payment_status');
                $this->load->view('include/footer');
            }else{
                 echo '{"OrderStatus": [{"status":"1"}]}';
                $this->load->view('include/header_for_nonlogin',$this->data);
                $this->load->view('payment_status');
                $this->load->view('include/footer_for_nonlogin');
            }


        }else if($insertTransaction == 'failed'){
            $this->data['is_success'] = false;
            $this->data['title'] = 'An error occured';
            $this->data['status'] = 'Payment Successfully Placed but there something went wrong.';
            $this->data['status_details'] = 'Payment Successfully placed with ID - '.$result->transaction->id.' But there are some technical issues occured. Please keep Payment ID and contact with us.';
            if($this->ion_auth->logged_in()){
                $this->load->view('include/header',$this->data);
                $this->load->view('payment_status');
                $this->load->view('include/footer');
            }else{
                $this->load->view('include/header_for_nonlogin',$this->data);
                $this->load->view('payment_status');
                $this->load->view('include/footer_for_nonlogin');
            }

        }
            
        }
        } 
        else if ($result->transaction) 
        {
         $this->data['title'] = 'Payment canceled';
        $this->data['status'] = 'Payment canceled';
        $this->data['status_details'] = 'Transaction has been failed, please use other card.';
        if($this->ion_auth->logged_in()){
            
            $this->load->view('include/header',$this->data);
            $this->load->view('payment_status_cancel');
            $this->load->view('include/footer');
        }else{
           
            $this->load->view('include/header_for_nonlogin',$this->data);
            $this->load->view('payment_status_cancel');
            $this->load->view('include/footer_for_nonlogin');
        }
       // echo '{"OrderStatus": [{"status":"2"}]}';
        } 
        else 
        {
        $this->data['title'] = 'Payment canceled';
        $this->data['status'] = 'Payment canceled';
        $this->data['status_details'] = 'Card number is not valid, please use other card.';
        if($this->ion_auth->logged_in()){
            
            $this->load->view('include/header',$this->data);
            $this->load->view('payment_status_cancel');
            $this->load->view('include/footer');
        }else{
           
            $this->load->view('include/header_for_nonlogin',$this->data);
            $this->load->view('payment_status_cancel');
            $this->load->view('include/footer_for_nonlogin');
        }
        //echo '{"OrderStatus": [{"status":"0"}]}';
        }
    }
}
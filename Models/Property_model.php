<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Property_model extends CI_Model

{

	public function __construct()

	{

		$this->config->load('ion_auth', TRUE);

        $this->load->database();
        
		$this->load->helper('cookie');

		$this->load->helper('date');

        $this->lang->load('ion_auth');

    }



    public function add_property($property_data){

        $current_user_id = $this->ion_auth->user()->row()->id;
       
        $cityName = $this->ion_auth->get_city_name_with_id($property_data['property_city']);
        $stateName=$this->ion_auth->get_state_name_with_id($property_data['property_state']);
        $countryName=$this->ion_auth->get_country_name_with_id($property_data['property_country']);

        $property_data['property_slug'] = $this->make_unique_property_slug($property_data['property_title'].' '.$cityName.' '.$stateName.' '.$countryName);

        $property_data['property_added_by'] = $current_user_id;

        $property_data['created_by'] = $current_user_id;

        $property_data['modified_by'] = $current_user_id;
        $property_data['modified_by_admin'] = $this->ion_auth->admin_id();

        $this->db->insert('properties_details', $property_data);

        
        $insert_id = $this->db->insert_id();
        
        return $insert_id;
     
    }

    public function make_unique_property_slug($str, $delimiter = '-'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        
        $query = $this->db->get_where('properties_details', array('property_slug' => $slug), 1);
        if($query->num_rows() == 0){
            return $slug;
        }else{
           for ($x = 1; $x <= 9999; $x++) {
                $new_slug = $slug.'-'.$x;
                if($this->is_unique_slug($new_slug)){
                    return $new_slug;
                    break;
                }
            } 
            

        }
        
    }

    public function is_unique_slug($slug){
        $query = $this->db->get_where('properties_details', array('property_slug' => $slug), 1);
        if($query->num_rows() == 0){
            return true;
        }else{
            return false;
        }
    }


    
    public function edit_property($property_data,$property_id){

        $current_user_id = $this->ion_auth->user()->row()->id;
        $property_data['property_description_is_approved'] = '0';
        
        $property_data['modified_by'] = $current_user_id;
        $property_data['modified_by_admin'] = $this->ion_auth->admin_id();

        //$this->db->insert('properties_details', $property_data);

        $this->db->where('property_id', $property_id);
        $this->db->update('properties_details', $property_data);

        return true;
     
    }


    
    public  function get_users_listings($user_id){
        $this->db->where('property_added_by', $user_id); 
        $query = $this->db->get('properties_details');
        if ($query->num_rows() > 0)
		{
                return $query->result();
        }else{
            return 'No result found';
        }
    }


    public function get_property_events($property_id){
        $this->db->where('property_id', $property_id); 
        $this->db->where('is_delete', '0'); 
        $query = $this->db->get('properties_events');
        if ($query->num_rows() > 0)
		{
                return $query->result();
        }else{
            return FALSE;
        }
    }

    

    public function get_properties_availability($property_id){
        $this->db->where('property_id', $property_id); 
        $this->db->where('is_delete', '0'); 
        $this->db->order_by("from_date", "desc");
        $query = $this->db->get('properties_availability');
        if ($query->num_rows() > 0)
		{
                return $query->result();
        }else{
            return FALSE;
        }
    }

    public function get_properties_booked_dates($property_id){
        $this->db->where('property_id', $property_id); 
        $this->db->where('is_delete', '0'); 
        $this->db->where('is_active', '1'); 
        
        $query = $this->db->get('booking_trip_details');
        if ($query->num_rows() > 0)
		{
                return $query->result();
        }else{
            return FALSE;
        }
    }

    public function get_property_details($property_id){
       // $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
        $this->db->where('property_id', $property_id);
        //$this->db->where('property_added_by',$current_logged_in_user_id); 
        $query = $this->db->get('properties_details');
        if ($query->num_rows() == 1)
		{
            foreach($query->result() as $result_set){
                return $result_set;
            }
        }else{
            return FALSE;
        }
    }
    public function get_property_details_array($property_id){
        $this->db->where('property_id', $property_id); 
        $query = $this->db->get('properties_details');
        if ($query->num_rows() == 1)
		{
            foreach($query->result_array() as $result_set){
                return $result_set;
            }
        }else{
            return FALSE;
        }
    }
    public function get_property_name_n_image_by_id($property_id){
        $this->db->select('property_title,property_slug,property_cover_photo,property_cover_photo_is_approved');
        $this->db->where('property_id', $property_id); 
        $query = $this->db->get('properties_details');
        if ($query->num_rows() == 1)
		{
            foreach($query->result() as $result_set){
                if($result_set->property_cover_photo_is_approved == 0){
                    $result_set->property_cover_photo ='';
                }   
                return $result_set;
            }
        }else{
            return FALSE;
        }
    }

    
    public function get_property_images($property_id){
        $this->db->where('property_id', $property_id); 
        $this->db->where('is_delete', '0'); 
        $query = $this->db->get('properties_images');
        if ($query->num_rows() == 0)
		{
            return FALSE;
        }else{
            return $query->result();
        }
    }

    function get_opt_amenities_list($property_id){
        $this->db->where('is_active', '1'); 
        $this->db->where('is_delete', '0'); 
        $query = $this->db->get('amenities');
        $result_set = array();
        if ($query->num_rows() == 0)
		{
            return FALSE;
        }else{
            // return $query->result_array();
            foreach($query->result_array() as $amenity){
                $if_amenity_exist_in_property = $this->get_properties_amenties_list($property_id,$amenity['amenity_id']);
                if(is_array($if_amenity_exist_in_property)){
                    $amenity['is_added'] = '1';
                    $result_set[] = $amenity;
                }else{
                    $amenity['is_added'] = '0';
                    $result_set[] = $amenity;
                }
            }
            return  $result_set;
        }
    }

    

    function get_opt_amenities_others_list($property_id){
        $this->db->where('is_active', '1'); 
        $this->db->where('is_delete', '0'); 
        $query = $this->db->get('amenities_others');
        $result_set = array();
        if ($query->num_rows() == 0)
		{
            return FALSE;
        }else{
            // return $query->result_array();
            foreach($query->result_array() as $amenity){
                $if_amenity_exist_in_property = $this->get_properties_amenties_others_list($property_id,$amenity['other_amenity_id']);
                if(is_array($if_amenity_exist_in_property)){
                    $amenity['is_added'] = '1';
                    $result_set[] = $amenity;
                }else{
                    $amenity['is_added'] = '0';
                    $result_set[] = $amenity;
                }
            }
            return  $result_set;
        }
    }



    function get_events_of_property($property_id){
        $this->db->where('is_active', '1'); 
        $this->db->where('is_delete', '0'); 
        $this->db->where('property_id', $property_id); 
        $query = $this->db->get('properties_events');
        $result_set = array();
        if ($query->num_rows() == 0)
		{
            return FALSE;
        }else{
            return $query->result_array();
        }
    }



    function get_properties_amenties_list($property_id,$amenities_id){
        $this->db->where('property_id', $property_id); 
        $this->db->where('amenities_id', $amenities_id); 
        
        $query = $this->db->get('properties_amenties');
        if ($query->num_rows() == 0)
		{
            return FALSE;
        }else{
            return $query->result();
        }
    }

    function get_properties_amenties_others_list($property_id,$amenities_others_id){
        $this->db->where('property_id', $property_id); 
        $this->db->where('amenities_others_id', $amenities_others_id); 
        
        $query = $this->db->get('properties_amenties_others');
        if ($query->num_rows() == 0)
		{
            return FALSE;
        }else{
            return $query->result();
        }
    }


    function getData($loadType,$loadId){
		if($loadType=="state"){
			$fieldList='id,name as name';
			$table='opt_loc_states';
			$fieldName='country_id';
			$orderByField='name';						
		}else{
			$fieldList='id,name as name';
			$table='opt_loc_cities';
			$fieldName='state_id';
			$orderByField='name';	
		}
		
		$this->db->select($fieldList);
		$this->db->from($table);
		$this->db->where($fieldName, $loadId);
		$this->db->order_by($orderByField, 'asc');
		$query=$this->db->get();
		return $query; 
    }
    
    function delete_property_image($image_id){
        $this->db->where('image_id', $image_id);
        
        if($this->db->update('properties_images',array('is_delete'=>'1'))){
            return TRUE;
        }else{
            return FALSE;
        }
    }


    public function add_new_price($price,$actual_price,$event_id,$from_date,$to_date,$segment_property_id,$current_logged_in_user_id){
        $price_data['price'] = $price;
        $price_data['actual_price'] = $actual_price;
        $price_data['property_id'] = $segment_property_id;
        if($event_id !== ''){
            $price_data['event_id'] = $event_id;
        }
       
        $price_data['from_date'] = $this->change_date_format($from_date);
        $price_data['to_date'] = $this->change_date_format($to_date);
       
        $price_data['created_by'] = $current_logged_in_user_id;
        $price_data['modified_by'] = $current_logged_in_user_id;
        $price_data['modified_by_admin'] = $this->ion_auth->admin_id();
       
        
        if($this->db->insert('properties_events_price', $price_data))
        {
            // Code here after successful insert
            return true;   // to the controller
        }else{return false;}
    }

    public function add_new_event($event_name,$from_date,$to_date,$event_description,$event_cover_image,$segment_property_id,$current_logged_in_user_id){
        
        $event_data['property_id'] = $segment_property_id;
        $event_data['event_name'] = $event_name;
      
 
        
        $event_data['from_date'] = $this->change_date_format($from_date);
        $event_data['to_date'] = $this->change_date_format($to_date);
        $event_data['event_description'] = $event_description; 
        $event_data['created_by'] = $current_logged_in_user_id;
        $event_data['modified_by'] = $current_logged_in_user_id;
        $event_data['modified_by_admin'] = $this->ion_auth->admin_id();
        $event_data['event_cover_image'] = $event_cover_image;

        
        if($this->db->insert('properties_events', $event_data))
        {
            // Code here after successful insert
            return true;   // to the controller
        }else{return false;}
    }

    
    
    public function add_new_availability($availability_data){

        $availability_data['from_date'] = $this->change_date_format($availability_data['from_date']);
        $availability_data['to_date'] = $this->change_date_format($availability_data['to_date']);
        
        if($this->db->insert('properties_availability', $availability_data))
        {
            // Code here after successful insert
            return true;   // to the controller
        }else{return false;}
    }

    public function edit_availability($from_date,$to_date,$segment_availability_id,$current_logged_in_user_id,$description){
        $availability_data['from_date'] = $this->change_date_format($from_date);
        $availability_data['to_date'] = $this->change_date_format($to_date);
        $availability_data['description'] = $description;
        
        $availability_data['modified_by'] = $current_logged_in_user_id;
        $availability_data['modified_by_admin'] = $this->ion_auth->admin_id();
             
        $this->db->where('id', $segment_availability_id);
        if($this->db->update('properties_availability', $availability_data))
        {
            // Code here after successful insert
            return true;   // to the controller
        }else{return false;}
    }

    public function edit_price($event_id,$from_date,$to_date,$segment_price_id,$segment_property_id,$current_logged_in_user_id,$price,$actual_price){
        
        if($event_id !== ''){
            $event_data['event_id'] = $event_id;
        }else{
            $event_data['event_id'] = NULL;
        }
        
        $event_data['property_id'] = $segment_property_id;
        $event_data['from_date'] = $this->change_date_format($from_date);
        $event_data['to_date'] = $this->change_date_format($to_date);
        $event_data['price'] = $price;
        $event_data['actual_price'] = $actual_price;

        
        $event_data['modified_by'] = $current_logged_in_user_id;
        $event_data['created_by'] = $current_logged_in_user_id;
        $event_data['modified_by_admin'] = $this->ion_auth->admin_id();
        $event_data_update['is_old'] = '1';
        $this->db->where('price_id', $segment_price_id);
        if($this->db->update('properties_events_price', $event_data_update))
        {

            return $this->add_new_price($price,$actual_price,$event_id,$from_date,$to_date,$segment_property_id,$current_logged_in_user_id);
            // Code here after successful insert
           // return true;   // to the controller
        }else{return false;}
    }

    public function change_date_format($date){
        // return date("Y-m-d", strtotime($date));
        return $this->change_date_format_with_english_month($date);
        
    }
    public function change_date_format_with_english_month($date){
        
       
        $date_to_date = date_create_from_format('j F, Y', $date);
       

        return @date_format($date_to_date, 'Y-m-d');
        
    }

    public function edit_event($event_name,$from_date,$to_date,$event_description,$event_cover_image,$segment_event_id,$current_logged_in_user_id){
        
        
        $event_data['event_name'] = $event_name;
        $event_data['from_date'] = $this->change_date_format_with_english_month($from_date);
        $event_data['to_date'] = $this->change_date_format_with_english_month($to_date);
        $event_data['event_description'] = $event_description; 
        $event_data['event_description_is_approved'] = '0';
        $event_data['modified_by'] = $current_logged_in_user_id;
        $event_data['modified_by_admin'] = $this->ion_auth->admin_id();
        if($event_cover_image !== ''){
            $event_data['event_cover_image_is_approved'] = '0';
            $event_data['event_cover_image'] = $event_cover_image;
        }

        
        $this->db->where('event_id', $segment_event_id);
        if($this->db->update('properties_events', $event_data))
        {
            // Code here after successful insert
            return true;   // to the controller
        }else{return false;}
    }

    function toggle_active_this_event($event_id,$is_active){
        $this->db->where('event_id', $event_id);
        if($this->db->update('properties_events',array('is_active'=>$is_active))){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function toggle_active_this_price($price_id,$is_active){
        $this->db->where('price_id', $price_id);
        if($this->db->update('properties_events_price',array('is_active'=>$is_active))){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function toggle_active_this_property($property_id,$is_active){
        $this->db->where('property_id', $property_id);
        if($this->db->update('properties_details',array('is_active'=>$is_active))){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function delete_event($event_id){
        $this->db->where('event_id', $event_id);
        if($this->db->update('properties_events',array('is_delete'=>'1'))){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function delete_price($id){
        $this->db->where('price_id', $id);
        if($this->db->update('properties_events_price',array('is_delete'=>'1'))){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function delete_availability($availability_id){
        $this->db->where('id', $availability_id);
        if($this->db->update('properties_availability',array('is_delete'=>'1'))){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function add_remove_amenity_from_property($property_id,$amenities_id,$state){
       
        if($state == 'remove'){
            $this->db->where('property_id', $property_id);
            $this->db->where('amenities_id', $amenities_id);
            if($this->db->delete('properties_amenties')){
                return TRUE;
                // echo "removed";
            }else{
                return FALSE;
                // echo "error while removed";
            }
        }elseif($state == 'add'){
            $properties_amenties_data['property_id'] = $property_id;
            $properties_amenties_data['amenities_id'] = $amenities_id;
            $properties_amenties_data['created_by'] = $this->ion_auth->user()->row()->id;
            $properties_amenties_data['modified_by'] = $this->ion_auth->user()->row()->id;
            $properties_amenties_data['modified_by_admin'] = $this->ion_auth->admin_id();
            if($this->db->insert('properties_amenties', $properties_amenties_data))
            {
                return true;
                // echo "insert true";
            }else{
                return false;
                // echo "insert false";
            }
        }else{
            return FALSE;
            // echo "state false";
        }
    }




    function add_remove_amenity_other_from_property($property_id,$amenities_id,$state){
       
        if($state == 'remove'){
            $this->db->where('property_id', $property_id);
            $this->db->where('amenities_others_id', $amenities_id);
            if($this->db->delete('properties_amenties_others')){
                return TRUE;
                // echo "removed";
            }else{
                return FALSE;
                // echo "error while removed";
            }
        }elseif($state == 'add'){
            $properties_amenties_data['property_id'] = $property_id;
            $properties_amenties_data['amenities_others_id'] = $amenities_id;
            $properties_amenties_data['created_by'] = $this->ion_auth->user()->row()->id;
            $properties_amenties_data['modified_by'] = $this->ion_auth->user()->row()->id;
            $properties_amenties_data['modified_by_admin'] = $this->ion_auth->admin_id();
            if($this->db->insert('properties_amenties_others', $properties_amenties_data))
            {
                return true;
                // echo "insert true";
            }else{
                return false;
                // echo "insert false";
            }
        }else{
            return FALSE;
            // echo "state false";
        }
    }


    function get_properties_single_event($property_id,$property_pricing_event_id){
        $this->db->where('property_id', $property_id); 
        $this->db->where('event_id', $property_pricing_event_id); 
        // $this->db->where('is_active', '1'); 
        // $this->db->where('is_delete', '0'); 
        
        $query = $this->db->get('properties_events');
        if ($query->num_rows() == 0)
		{
            return FALSE;
        }else{
            return $query->result_array();
        }
    }

    function get_property_pricing($property_id){
        //$this->db->where('is_active', '1'); 
        $this->db->where('is_delete', '0'); 
        $this->db->where('is_old', '0'); 
        $this->db->where('property_id', $property_id); 
        $query = $this->db->get('properties_events_price');
        $result_set = array();
        if ($query->num_rows() == 0)
		{
            return FALSE;
        }else{
            // return $query->result_array();
            foreach($query->result_array() as $property_pricing){
                if($property_pricing['event_id'] == ''){
                    $property_pricing['event_name'] = NULL;
                    $property_pricing['event_description'] = NULL;
                    $property_pricing['event_is_active'] = NULL;
                    $property_pricing['event_is_delete'] = NULL;
                    $property_pricing['event_cover_image'] = NULL;
                    
                    
                }else{
                    $if_property_pricing_exist_in_property = $this->get_properties_single_event($property_id,$property_pricing['event_id']);
                    //$property_pricing['event_array'] = $if_property_pricing_exist_in_property;
                    if(is_array($if_property_pricing_exist_in_property)){
                        $property_pricing['event_name'] = $if_property_pricing_exist_in_property[0]['event_name'];
                        $property_pricing['event_description'] = $if_property_pricing_exist_in_property[0]['event_description'];
                        $property_pricing['event_is_active'] = $if_property_pricing_exist_in_property[0]['is_active'];
                        $property_pricing['event_is_delete'] = $if_property_pricing_exist_in_property[0]['is_delete'];
                        $property_pricing['event_cover_image'] = $if_property_pricing_exist_in_property[0]['event_cover_image'];
                        
                    }

                    
                }

                    $result_set[] = $property_pricing;                
            }
            return  $result_set;
        }
    }

    public function my_total_earning($for_month=null){
        $my_property_ids= $this->get_host_properties_ids();
        $this->db->select('SUM(price_at_the_time_of_booking) as total_earning', false);
        if($for_month!==null){
            $current_date = date("Y-m-d");
            $current_datetime = date("Y-m-d H:i:s");
            if($for_month == 'last'){
                $fdolm=date_create($current_date.' first day of last month');
                $ldolm=date_create($current_date.' last day of last month');
                $date_fdolm = '';
                $date_fdolm .= $fdolm->format('Y-m-d H:i:s');
                $date_ldolm = '';
                $date_ldolm .= $ldolm->format('Y-m-d H:i:s');
                
                $this->db->where('created_date >=', $date_fdolm);
                $this->db->where('created_date <=', $date_ldolm);
                
            }else if($for_month == 'current'){

                $fdotm = new DateTime('first day of this month');
                $date_fdotm = '';
                $date_fdotm .= $fdotm->format('Y-m-d H:i:s');

                $date_ldotm = $current_datetime;
                $this->db->where('created_date >=', $date_fdotm);
                $this->db->where('created_date <=', $current_datetime);
            }else{
                
            }
        }
       
        $this->db->where_in('property_id', $my_property_ids); 
        $query = $this->db->get('booking_trip_details');
        
        if ($query->num_rows() == 0)
		{
            return false;
        }else{
            // foreach($query->result_array() as $property_id_obj){
            //         $result_set[] = $property_id_obj['property_id'];
            //     }
            //     return $result_set;
            // echo "<pre>";     print_r($query->result()['0']->total_earning);       echo "</pre>";die('sa');
            return $query->result()['0']->total_earning;
        }
    }


    public function my_total_upcoming_events(){
        $my_property_ids= $this->get_host_properties_ids();
        $this->db->where('is_active', '1'); 
        $this->db->where('is_delete', '0'); 
        $this->db->where_in('property_id', $my_property_ids); 
        $current_date = date("Y-m-d");
        $this->db->where('to_date >=', $current_date);
        $query = $this->db->get('properties_events');
        return $query->num_rows();
    }

    public function my_total_booked_properties($status=null){
        $my_property_ids= $this->get_host_properties_ids();
        $this->db->where_in('property_id', $my_property_ids); 
        if($status!==null){
            $this->db->where('status', $status); 
        }
        $query = $this->db->get('booking_trip');
        
            return $query->num_rows();
            
        
    }

    function get_all_upcoming_events_of_hosts_properties(){
        $my_property_ids= $this->get_host_properties_ids();
        $this->db->where('is_active', '1'); 
        $this->db->where('is_delete', '0'); 
        $this->db->where_in('property_id', $my_property_ids); 

        $current_date = date("Y-m-d");
        $this->db->where('to_date >=', $current_date);
        // $this->db->where('created_date <=', $current_datetime);


        $query = $this->db->get('properties_events');
        $result_set = array();
        if ($query->num_rows() == 0)
		{
            return FALSE;
        }else{
            return $query->result();
        }
    }

    public function get_host_properties_ids(){
        $result_set = array();
        $this->db->select('property_id'); 
        $this->db->where('property_added_by', $this->ion_auth->user()->row()->id); 
        $query = $this->db->get('properties_details');
        
        if ($query->num_rows() == 0)
		{
            return false;
        }else{
            foreach($query->result_array() as $property_id_obj){
                    $result_set[] = $property_id_obj['property_id'];
                }
                return $result_set;
        }
        
    }

    public function get_trip_details($booking_trip_id,$property_id){
        $this->db->where('booking_trip_id', $booking_trip_id); 
        $this->db->where('property_id', $property_id); 
                
        $query = $this->db->get('booking_trip_details');
        
        if ($query->num_rows() == 0)
        {
            return false;
        }else{
            return $query->result_array();
            
        } 
    }

    public function get_booking_details($booking_id){
      
        $this->db->where('booking_id', $booking_id); 
                
        $query = $this->db->get('booking_details');
        
        if ($query->num_rows() == 0)
        {
            return false;
        }else{
            return $query->result_array()[0];
            
        } 
    }

    public function get_host_booking_trips_of_properties($host_property_ids_array){
        
                
                $this->db->where_in('property_id', $host_property_ids_array); 
                
                $query = $this->db->get('booking_trip');
                
                if ($query->num_rows() == 0)
                {
                    return false;
                }else{
                    
                    // $result_set['host_property_ids_array'] = $host_property_ids_array;
                    // $result_set['bookin_details']= '';
                    // $result_set['booking_trips_of_properties'] = array();
                    
                    //return $query->result_array();
                    foreach($query->result_array() as $booking_trip){
                        $result_set = array();
                        if(empty($result_set['bookin_details'])){
                            $result_set['bookin_details']=$this->get_booking_details($booking_trip['booking_id']);
                            $result_set['bookin_details']['paid_by_user_first_name']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->first_name;
                            $result_set['bookin_details']['paid_by_user_last_name']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->last_name;
                            $result_set['bookin_details']['paid_by_user_email']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->email;
                            $result_set['bookin_details']['paid_by_user_phone']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->phone;
                            $result_set['bookin_details']['paid_by_user_image']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->image;

                            $result_set['bookin_details']['paid_by_user_street_address]']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->street_address;
                            $result_set['bookin_details']['paid_by_user_city']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->city;
                            $result_set['bookin_details']['paid_by_user_state']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->state;
                            $result_set['bookin_details']['paid_by_user_zip_code']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->zip_code;
                            $result_set['bookin_details']['paid_by_user_country']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->country;
                            $result_set['bookin_details']['paid_by_user_contact_no']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->contact_no;
                            $result_set['bookin_details']['paid_by_user_gender']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->gender;
                            $result_set['bookin_details']['paid_by_user_marital_status']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->marital_status;
                            $result_set['bookin_details']['paid_by_user_d_o_b']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->d_o_b;
                            $result_set['bookin_details']['paid_by_user_created_date']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->created_date;

                        }
                        
                        $bookings[$booking_trip['booking_id']] = array();

                        $booking_trip['booking_properties_trip_details']=$this->get_trip_details($booking_trip['booking_trip_id'],$booking_trip['property_id']);
                        // $result_set['booking_trips_of_properties'][] = $booking_trip;
                        $result_set['bookin_details']['booking_properties_trips'][] =  $booking_trip; 
                        $bookings[$booking_trip['booking_id']] =  $result_set;
                    }
                    return $bookings;
                }
               
    }

    public function get_host_bookings(){
        return $this->get_host_booking_trips_of_properties($this->get_host_properties_ids());
    }
    public function get_guest_bookings(){
        $this->db->where('paid_by_user_id', $this->ion_auth->user()->row()->id); 
        $query_booking_details = $this->db->get('booking_details');
        $booking_result_set = array();
        if ($query_booking_details->num_rows() > 0)
            {
                foreach($query_booking_details->result_array() as $booking_result){
                    
                    $this->db->where('booking_id', $booking_result['booking_id']); 
                    $query_booking_trip = $this->db->get('booking_trip');
            
                    if ($query_booking_trip->num_rows() > 0)
                    {
                       
                        foreach($query_booking_trip->result_array() as $booking_trip_result){
                            
                            $this->db->where('booking_trip_id', $booking_trip_result['booking_trip_id']); 
                            $query_booking_trip_details = $this->db->get('booking_trip_details');

                            if ($query_booking_trip_details->num_rows() > 0)
                            {
                                foreach($query_booking_trip_details->result_array() as $query_booking_trip_details_result)
                                {
                                    $booking_trip_result['booking_trip_details'][]=$query_booking_trip_details_result;
                                    
                                }
                                $booking_result['booking_trip'][] = $booking_trip_result;
                               
                            }
                        }
                    }
                    $booking_result_set[$booking_result['booking_id']] = array();
                $booking_result_set[$booking_result['booking_id']] = $booking_result;
                }
                
            }
            return $booking_result_set;
    }



    public function get_guest_total_bookings(){
      
        $this->db->where('paid_by_user_id', $this->ion_auth->user()->row()->id); 
        // $this->db->where('status', 'success'); 
        $query_booking_details = $this->db->get('booking_details');
        return $query_booking_details->num_rows();
    }

    public function get_guest_total_spend_till_now($for_month=null){
        
        $this->db->select('SUM(amount_paid) as amount_paid', false);
        if($for_month!==null){
            $current_date = date("Y-m-d");
            $current_datetime = date("Y-m-d H:i:s");
            if($for_month == 'last'){
                $fdolm=date_create($current_date.' first day of last month');
                $ldolm=date_create($current_date.' last day of last month');
                $date_fdolm = '';
                $date_fdolm .= $fdolm->format('Y-m-d H:i:s');
                $date_ldolm = '';
                $date_ldolm .= $ldolm->format('Y-m-d H:i:s');
                
                $this->db->where('created_date >=', $date_fdolm);
                $this->db->where('created_date <=', $date_ldolm);
                
            }else if($for_month == 'current'){

                $fdotm = new DateTime('first day of this month');
                $date_fdotm = '';
                $date_fdotm .= $fdotm->format('Y-m-d H:i:s');

                $date_ldotm = $current_datetime;
                $this->db->where('created_date >=', $date_fdotm);
                $this->db->where('created_date <=', $current_datetime);
            }else{
                
            }
        }
       
        $this->db->where('paid_by_user_id', $this->ion_auth->user()->row()->id);
        $statuses = array('booked',
        'success',
        'pending',
        'partial_pending',
        'canceled_by_guest',
        'partial_canceled_by_host');

        $this->db->where_in('status', $statuses); 
        $query = $this->db->get('booking_details');
        
        if ($query->num_rows() == 0)
		{
            return false;
        }else{
            // foreach($query->result_array() as $property_id_obj){
            //         $result_set[] = $property_id_obj['property_id'];
            //     }
            //     return $result_set;
            // echo "<pre>";     print_r($query->result()['0']->total_earning);       echo "</pre>";die('sa');
            return $query->result()['0']->amount_paid;
        }
    }





    public function get_host_booking_by_id($id){

        $result_set = array();
        $result_set['bookin_details'] = array();
        $result_set['bookin_trips'] = array();
        

        $result_set['bookin_details']=$this->get_booking_details($id);
        if(empty($result_set['bookin_details'])){
            return false;
        }
       // echo "<pre>"; print_r($result_set['bookin_details']); echo "</pre>"; die('sa');
        $result_set['bookin_details']['paid_by_user_first_name']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->first_name;
        $result_set['bookin_details']['paid_by_user_last_name']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->last_name;
        $result_set['bookin_details']['paid_by_user_email']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->email;
        $result_set['bookin_details']['paid_by_user_phone']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->phone;
        $result_set['bookin_details']['paid_by_user_image']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->image;

        $result_set['bookin_details']['paid_by_user_street_address]']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->street_address;
        $result_set['bookin_details']['paid_by_user_city']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->city;
        $result_set['bookin_details']['paid_by_user_state']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->state;
        $result_set['bookin_details']['paid_by_user_zip_code']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->zip_code;
        $result_set['bookin_details']['paid_by_user_country']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->country;
        $result_set['bookin_details']['paid_by_user_contact_no']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->contact_no;
        $result_set['bookin_details']['paid_by_user_gender']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->gender;
        $result_set['bookin_details']['paid_by_user_marital_status']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->marital_status;
        $result_set['bookin_details']['paid_by_user_d_o_b']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->d_o_b;
        $result_set['bookin_details']['paid_by_user_created_date']=$this->ion_auth->user($result_set['bookin_details']['paid_by_user_id'])->row()->created_date;
        



        $this->db->where_in('property_id', $this->get_host_properties_ids()); 
        $this->db->where('booking_id', $id); 
        $query_booking_trip = $this->db->get('booking_trip');
        if ($query_booking_trip->num_rows() > 0)
        {
           
            $query_booking_trip_array_result = $query_booking_trip->result_array();
            
            
            foreach($query_booking_trip_array_result as $booking_trip){
                $result_set['bookin_trips'][$booking_trip['property_id']] = $booking_trip;
                $property_id = $booking_trip['property_id'];
                $result_set['bookin_trips'][$booking_trip['property_id']]['bookin_trip_property_details']=$this->get_property_details($property_id);
                
                $result_set['bookin_trips'][$booking_trip['property_id']]['bookin_trips_details']=$this->get_trip_details($booking_trip['booking_trip_id'],$booking_trip['property_id']);
                
            }
          
        }
        return $result_set;
        
    }




    
    public function get_guest_booking_by_id($id){

        $result_set = array();
        $result_set['bookin_details'] = array();
        $result_set['bookin_trips'] = array();
        

        $result_set['bookin_details']=$this->get_booking_details($id);
        if(empty($result_set['bookin_details'])){
            return false;
        }
         
        $current_logged_in_user_id = $this->ion_auth->user()->row()->id;
        $this->db->where('booking_id', $id);
        $this->db->where('book_by_user_id', $current_logged_in_user_id); 
        $query_booking_trip = $this->db->get('booking_trip');
        if ($query_booking_trip->num_rows() > 0)
        {
           
            $query_booking_trip_array_result = $query_booking_trip->result_array();
            
            
            foreach($query_booking_trip_array_result as $booking_trip){
               
                $result_set['bookin_trips'][$booking_trip['property_id']] = $booking_trip;
                $property_id = $booking_trip['property_id'];
                $result_set['bookin_trips'][$booking_trip['property_id']]['bookin_trip_property_details']=$this->get_property_details($property_id);
                
                $result_set['bookin_trips'][$booking_trip['property_id']]['bookin_trips_details']=$this->get_trip_details($booking_trip['booking_trip_id'],$booking_trip['property_id']);
                
            }
          
        }
        return $result_set;
        
    }

    public function get_property_amenities_fields_childs($amenities_field_id){
        $this->db->where('is_active', '1'); 
                $this->db->where('is_old', '0'); 
                $this->db->where('is_delete', '0'); 
                $this->db->where('amenities_field_id', $amenities_field_id); 
                $query = $this->db->get('amenities_fields_childs');
                if ($query->num_rows() == 0)
                {
                    return false;
                }else{
                    return $query->result_array();
                }
    }

    public function get_amenities_fields_id_from_child_field_id($child_field_id){
        $this->db->select('amenities_field_id');
        $this->db->where('amenities_field_child_id', $child_field_id); 
        $query = $this->db->get('amenities_fields_childs');
        if ($query->num_rows() == 0)
		{
            return FALSE;   
        }else{
            return $query->result_array()[0]['amenities_field_id'];
        }
    }

    public function add_properties_amenties_fields($properties_amenties_fields_data,$property_id){
        if($this->make_old_properties_amenties_fields($property_id)){
            return $this->db->insert_batch('properties_amenties_fields', $properties_amenties_fields_data); 
        }
        
    }

    public function make_old_properties_amenties_fields($property_id){
        $properties_amenties_fields_data['is_old'] = '1';
        $this->db->where('property_id', $property_id);
        $this->db->where('is_old', '0');
        return $this->db->update('properties_amenties_fields', $properties_amenties_fields_data);
    }

    
    public function get_property_amenities_fields_of_property($property_id){
        $this->db->where('is_active', '1'); 
        $this->db->where('is_old', '0'); 
        $this->db->where('is_delete', '0'); 
        $this->db->where('property_id', $property_id); 
        $query = $this->db->get('properties_amenties_fields');
        if ($query->num_rows() == 0)
		{
            return FALSE;   
        }else{
            return $query->result_array() ;
        }
    }

    public function get_property_amenities_fields(){
        $this->db->where('is_active', '1'); 
        $this->db->where('is_old', '0'); 
        $this->db->where('is_delete', '0'); 
      //  $this->db->order_by('amenities_field_id','2','ASC');
        $this->db->order_by('FIELD (amenities_field_id,2)');
        $query = $this->db->get('amenities_fields');
        if ($query->num_rows() == 0)
		{
            return FALSE;   
        }else{
            $result_set = array();
             foreach( $query->result_array() as $amenities_fields){
                $amenities_field_id = $amenities_fields['amenities_field_id'];
                // $single_result_set = array();
                // $single_result_set[$amenities_field_id] = $amenities_fields;
                $this->db->where('is_active', '1'); 
                $this->db->where('is_old', '0'); 
                $this->db->where('is_delete', '0'); 
                $this->db->where('amenities_field_id', $amenities_field_id); 
                $query = $this->db->get('amenities_fields_childs');
                if ($query->num_rows() == 0)
                {
                    $amenities_fields['amenities_fields_childs'] = array();
                }else{
                    $amenities_fields['amenities_fields_childs'] = $query->result_array();
                }
                $result_set[$amenities_field_id] = $amenities_fields;
             }
             return $result_set;
        }
    }



    public function get_recommended_props($count=99){
    

        // $this->db->where('amenities_field_child_id', $child_field_id); 
        $this->db->where('is_delete','0');
        $this->db->where('is_active','1');
        $this->db->where('is_recommended','1');
        // $this->db->group_by('property_country'); 
        $this->db->order_by('created_date', 'desc'); 
        // $this->db->limit(10,0);
        $query = $this->db->get('properties_details',$count);
        //  print_r($this->db->last_query());
        if ($query->num_rows() == 0)
		{
            return FALSE;   
        }else{
              
            // echo "<pre>"; print_r($query->result()); echo "</pre>";die('sadasdasdasd');
        
            return $query->result();
        }
    }

    
    public function get_search_props($search_criteria=null,$count=99,$search_cri_array=array(),$users=array(),$dates=null,$limit,$offset){
        if(empty($dates)){
                $from_date = '';
                $to_date = '';
        }else{
            $from_date = $this->ion_auth->change_date_format($dates['from_date']);
            $to_date =  $this->ion_auth->change_date_format($dates['to_date']);
        }
        
        $this->db->select("a.property_id,a.property_title,a.property_slug,a.property_cover_photo,a.property_cover_photo_is_approved,a.property_description,a.property_description_is_approved,a.property_address,a.property_street_address,a.property_city,a.property_state,a.property_zip_code,a.property_suburb,a.property_country,a.property_default_price,a.latitude,a.longitude,a.property_home_phone,a.property_work_phone,a.property_added_by,a.is_delete,a.is_active,a.is_recommended,a.created_by,a.created_date,a.modified_by,a.modified_date,b.from_date,b.to_date,
        CASE when (b.from_date = '$from_date' and b.to_date = '$to_date' and b.status = 'booked') Then 'Booked'
        when (b.from_date >= '$from_date' and b.to_date <= '$to_date' and b.status = 'booked') Then 'Partially Available'
        ELSE  'Available' end as status", FALSE );


        $this->db->from('properties_details a');

        $this->db->join('booking_trip b','a.property_id = b.property_id','left');
        // $this->db->join('properties_amenties pa','a.property_id = pa.property_id','left');
        // $this->db->group_by('pa.amenities_id'); 
        if(!empty($search_cri_array)){
            foreach($search_cri_array as $search_cri_one_key=>$search_cri_one_val){
                if($search_cri_one_val !== '-1' && $search_cri_one_val !== ''){
                    
                        $this->db->where('a.'.$search_cri_one_key,$search_cri_one_val);
                    
                }     
            }
        }
        // if($users !== null || $users !== ''){
        if(!empty($users)){
            $this->db->where_in('a.property_added_by',$users);
        }
        if($search_criteria !== NULL){
            $search_criteria_ar = explode(" ",$search_criteria);
            $srchsting = '';
            foreach($search_criteria_ar as $search_criteria_ar_one){
                //$this->db->or_like('a.property_title', $search_criteria_ar_one); 
                if($srchsting == ''){
                    $srchsting = "a.property_title LIKE '%".$search_criteria_ar_one."%'";
                    $srchsting .= "OR a.property_description LIKE '%".$search_criteria_ar_one."%'";
                }else{
                    $srchsting .= "OR a.property_title LIKE '%".$search_criteria_ar_one."%'";
                    $srchsting .= "OR a.property_description LIKE '%".$search_criteria_ar_one."%'";
                }
               
            }
            // $this->db->like('a.property_title', $search_criteria);
            $this->db->where("($srchsting)", NULL, FALSE);
        }
        
        $this->db->where('a.is_delete','0');
        $this->db->where('a.is_active','1');
        // $this->db->order_by('status', 'desc'); 
        $this->db->order_by('a.is_recommended', 'desc'); 
        $this->db->order_by('a.created_date', 'desc'); 
        $this->db->group_by('a.property_id'); 
        $this->db->limit($limit,$offset);
        $query = $this->db->get();
        // print_r($this->db->last_query());
        
        if ($query->num_rows() == 0)
		{  
            return FALSE;   
        }else{
         
            return $query->result();
        }
    }

    
    public function get_search_props_beta($search_criteria=null,$count=99,$search_cri_array=array(),$users=null,$dates=null){
        // echo "<pre>";print_r($search_cri_array); die('sad');
        if(!empty($search_cri_array)){
            foreach($search_cri_array as $search_cri_one_key=>$search_cri_one_val){
                if($search_cri_one_val !== '-1'){
                    $this->db->where($search_cri_one_key,$search_cri_one_val);
                }     
            }
        }
        if($users !== null){
            $this->db->where_in('property_added_by',$users);
        }
        if($search_criteria !== NULL){
            $this->db->like('property_title', $search_criteria);
        }
        
        $this->db->where('is_delete','0');
        $this->db->where('is_active','1');
        $this->db->order_by('is_recommended', 'desc'); 
        $this->db->order_by('created_date', 'desc'); 
        $query = $this->db->get('properties_details',$count);
        if ($query->num_rows() == 0)
		{
            return FALSE;   
        }else{
            return $query->result();
        }
    }



    public function get_propertyid_by_slugname($property_slug){
        $this->db->select('property_id');
        $this->db->where('property_slug',$property_slug);
        $this->db->where('is_delete','0');
        $this->db->where('is_active','1');
        
        $query = $this->db->get('properties_details');
        if ($query->num_rows() == 1)
		{
           return $query->result_array()[0]['property_id']; 
        }else{
            return FALSE;
        }
    }

    public function get_name_of_field_amenity_child($amenities_fields_childs_id){
        $this->db->where('is_active', '1'); 
        $this->db->where('is_old', '0'); 
        $this->db->where('is_delete', '0'); 
        $this->db->where('amenities_field_child_id', $amenities_fields_childs_id); 
        $query = $this->db->get('amenities_fields_childs');
        if ($query->num_rows() == 0)
		{
            return FALSE;   
        }else{
           // echo "<pre>";print_r($query->result_array());echo "</pre>";die('sadsadsadsad');
          return $query->result_array()[0]['amenities_field_name'];
        }
    }

    public function get_name_of_field_amenity($amenities_field_id){
        $this->db->where('is_active', '1'); 
        $this->db->where('is_old', '0'); 
        $this->db->where('is_delete', '0'); 
        $this->db->where('amenities_field_id', $amenities_field_id); 
        $query = $this->db->get('amenities_fields');
        if ($query->num_rows() == 0)
		{
            return FALSE;   
        }else{
           // echo "<pre>";print_r($query->result_array());echo "</pre>";die('sadsadsadsad');
          return $query->result_array()[0]['amenities_field_name'];
        }
    }

    public function get_property_amenities_fields_of_property_sorted_for_frontend_single_property($property_id){
        $this->db->where('is_active', '1'); 
        $this->db->where('is_old', '0'); 
        $this->db->where('is_delete', '0'); 
        $this->db->where('property_id', $property_id); 
        $query = $this->db->get('properties_amenties_fields');
        if ($query->num_rows() == 0)
		{
            return FALSE;   
        }else{
            
           
            
            $result_set =array();
            foreach($query->result_array() as $amt_fields){
                $name = $this->get_name_of_field_amenity($amt_fields['amenities_fields_id']);
                if($name){
                    $child_name = $this->get_name_of_field_amenity_child($amt_fields['amenities_fields_childs_id']);
                    $amt_fields['amenities_fields_name'] = $name;
                    $amt_fields['amenities_fields_childs_name'] = $child_name;
                    $result_set[$name][] = $amt_fields;
                }
                
            }
            return $result_set;
        }
    }



    public function get_amenities_fields_list(){
        $this->db->where('is_active', '1'); 
        $this->db->where('is_old', '0'); 
        $this->db->where('is_delete', '0'); 
        $query = $this->db->get('properties_amenties_fields');
        if ($query->num_rows() == 0)
		{
            return FALSE;   
        }else{
            
           
            
            $result_set =array();
            foreach($query->result_array() as $amt_fields){
                $name = $this->get_name_of_field_amenity($amt_fields['amenities_fields_id']);
                if($name){
                    $child_name = $this->get_name_of_field_amenity_child($amt_fields['amenities_fields_childs_id']);
                    $amt_fields['amenities_fields_name'] = $name;
                    $amt_fields['amenities_fields_childs_name'] = $child_name;
                    $result_set[$name][] = $amt_fields;
                }
                
            }
            return $result_set;
        }
    }


    public function get_property_amenities_fields_of_property_sorted_for_frontend_single_property_string_for_search_filter($property_id){
        $return_set = '';
        $this->db->where('is_active', '1'); 
        $this->db->where('is_old', '0'); 
        $this->db->where('is_delete', '0'); 
        $this->db->where('property_id', $property_id); 
        $query = $this->db->get('properties_amenties_fields');
        if ($query->num_rows() == 0)
		{
            return $return_set;   
        }else{
            
           
            
            $result_set =array();
            foreach($query->result_array() as $amt_fields){
                $name = $this->get_name_of_field_amenity($amt_fields['amenities_fields_id']);
                if($name){
                    $child_name = $this->get_name_of_field_amenity_child($amt_fields['amenities_fields_childs_id']);
                    $amt_fields['amenities_fields_name'] = $name;
                    $amt_fields['amenities_fields_childs_name'] = $child_name;
                    $result_set[$name][] = $amt_fields;
                }
                
            }
            
            if(is_array($result_set)){
                foreach($result_set as $ams_firstKey=>$ams_first){
                    $spacer = ' ';
                    $return_set .= $this->convert_string_to_cssclassname($ams_firstKey).$spacer;
                    foreach($ams_first as $ams_second){
                        $return_set .= $this->convert_string_to_cssclassname($ams_second['amenities_fields_childs_name']).$spacer;
                    }
                }
             
            }
        }
        return $return_set; 
    }
    
    public function convert_string_to_cssclassname($string_to_convert_in_class_name){
        $string_to_convert_in_class_name_return_1 = preg_replace('/\s+/', '_', strtolower($string_to_convert_in_class_name));
        $string_to_convert_in_class_name_return = preg_replace('/[^a-zA-Z0-9_]/', '_', $string_to_convert_in_class_name_return_1);
        return $string_to_convert_in_class_name_return;
    }

    public function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);
    
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
    
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
    
        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }


    public function is_property_hot($property_id=71){
         $this->db->where('property_id', $property_id);
         $this->db->order_by("created_date", "desc"); 
        $query = $this->db->get('booking_trip',1);
        if ($query->num_rows() == 0)
		{
            return 'This property never booked by any guest. Be the first one to get a fresh property.';   
        }else{
            return 'Last booked '.$this->time_elapsed_string($query->result_array()[0]['created_date']);
            
        }
    }


    public function get_property_details_by_slug($property_slug){
        
        $property_id = $this->get_propertyid_by_slugname($property_slug);
        if(!$property_id){
            redirect('home/search', 'refresh');
        }
        $property_details = array();
 
        $property_details['property_details'] = $this->Property_model->get_property_details($property_id);
        $property_details['property_images']  = $this->Property_model->get_property_images($property_id);
        $property_details['property_events']  = $this->Property_model->get_property_events($property_id);
        $property_details['properties_availability']  = $this->Property_model->get_properties_availability($property_id);
        $property_details['properties_booked_dates']  = $this->Property_model->get_properties_booked_dates($property_id);
        $property_details['opt_amenities_list']  = $this->Property_model->get_opt_amenities_list($property_id);
        $property_details['opt_amenities_others_list']  = $this->Property_model->get_opt_amenities_others_list($property_id);

        $property_details['opt_amenities_field_list']  = $this->Property_model->get_property_amenities_fields_of_property_sorted_for_frontend_single_property($property_id);

        $dates_inner_array_str = '';
        if($property_details['properties_availability'] !== false){
         foreach($property_details['properties_availability'] as $avails){
 
             $period = new DatePeriod(
                 new DateTime($avails->from_date),
                 new DateInterval('P1D'),
                 new DateTime(date('Y-m-d', strtotime('+1 day', strtotime($avails->to_date))))
            );
 
            foreach ($period as $key => $value) {
               if($dates_inner_array_str == ''){
                 $dates_inner_array_str = '"'.$value->format('d F, Y').'"';
               }else{
                 $dates_inner_array_str .= ',"'.$value->format('d F, Y').'"';
               }
             // $dates_inner_array_str[] = $value->format('Y-m-d');
         }
                 
                 
         }
        }
        
        
        
        $property_details['properties_availability_dates'] = '['.$dates_inner_array_str.']';  //'["2018/06/23","2018/06/25"]';
 

        if($property_details['properties_booked_dates'] !== false){
            foreach($property_details['properties_booked_dates'] as $avails){
    
                $period = new DatePeriod(
                    new DateTime($avails->date),
                    new DateInterval('P1D'),
                    new DateTime(date('Y-m-d', strtotime('+1 day', strtotime($avails->date))))
               );
    
               foreach ($period as $key => $value) {
                  if($dates_inner_array_str == ''){
                    $dates_inner_array_str = '"'.$value->format('d F, Y').'"';
                  }else{
                    $dates_inner_array_str .= ',"'.$value->format('d F, Y').'"';
                  }
                // $dates_inner_array_str[] = $value->format('Y-m-d');
            }
                    
                    
            }
           }
           $property_details['properties_disabled_dates'] = '['.$dates_inner_array_str.']';

           
        $property_details['properties_pricing'] = $this->Property_model->get_property_pricing($property_id);
        $property_details['properties_pricing_json_for_calender'] = $this->Property_model->get_json_string_of_property_pricing_for_calender_tooltips($property_id);
        $dates_inner_array_strperiod_price_dates = '';
        if($property_details['properties_pricing'] !== false){
         foreach($property_details['properties_pricing'] as $avails_price_dates){
 
             $period_price_dates = new DatePeriod(
                 new DateTime($avails_price_dates['from_date']),
                 new DateInterval('P1D'),
                 new DateTime(date('Y-m-d', strtotime('+1 day', strtotime($avails_price_dates['to_date']))))
            );
 
            foreach ($period_price_dates as $keyperiod_price_dates => $valueperiod_price_dates) {
               if($dates_inner_array_strperiod_price_dates == ''){
                 $dates_inner_array_strperiod_price_dates = '"'.$valueperiod_price_dates->format('d F, Y').'"';
               }else{
                 $dates_inner_array_strperiod_price_dates .= ',"'.$valueperiod_price_dates->format('d F, Y').'"';
               }
             // $dates_inner_array_str[] = $value->format('Y-m-d');
         }
                 
                 
         }
        }
        $property_details['properties_availability_dates_for_pricing'] = '['.$dates_inner_array_strperiod_price_dates.']';  //'["2018/06/23","2018/06/25"]';
 
 
        $property_details['properties_events_list'] = $this->Property_model->get_events_of_property($property_id);
        
        return $property_details;
     }




     public function get_dates_string_of_two_dates($dates){
        //   echo "<pre>"; print_r($dates); echo "</pre>";die('saddsadasd');
        $dates_inner_array_str = array();
        
        
 
             $period = new DatePeriod(
                 new DateTime($dates['from_date']),
                 new DateInterval('P1D'),
                 new DateTime(date('Y-m-d', strtotime('+1 day', strtotime($dates['to_date']))))
            );
 
            foreach ($period as $key => $value) {
                $dates_inner_array_str[] = $value->format('Y-m-d');
            }
                 
        return $dates_inner_array_str;
    }


    public function get_dates_array_of_two_dates($dates){
        //   echo "<pre>"; print_r($dates); echo "</pre>";die('saddsadasd');
        $dates_inner_array_str = array();
        
        
 
             $period = new DatePeriod(
                 new DateTime($dates['from_date']),
                 new DateInterval('P1D'),
                 new DateTime(date('Y-m-d', strtotime('+1 day', strtotime($dates['to_date']))))
            );
 
            foreach ($period as $key => $value) {
                $dates_inner_array_str[] = $value->format('Y-m-d');
            }
                 
        return $dates_inner_array_str;
    }

    public function get_status_property_for_given_search_dates($dates,$property_id){
        $date_string_from_to = $this->get_dates_string_of_two_dates($dates);
        $this->db->where_in('property_id', $property_id); 
        $this->db->where_in('is_active', '1'); 
        $this->db->where_in('is_delete', '0'); 
        $this->db->where_in('date', $date_string_from_to); 
        $query = $this->db->get('booking_trip_details');

        if($query->num_rows() == 0){
            return 'Available';
        }else if($query->num_rows() < count(array_filter($date_string_from_to, 'strlen'))){
            return 'Partial Available';
        }else if($query->num_rows() == count(array_filter($date_string_from_to, 'strlen'))){
            return 'Booked';
        }else{
            return 'Unknown';
        }
    }
    

    public function get_property_default_price($property_id){
        $this->db->select('property_default_price');
        $this->db->where('property_id', $property_id); 
        $this->db->where('is_delete','0');
        $this->db->where('is_active','1');
        $query = $this->db->get('properties_details');
        if ($query->num_rows() == 0)
        {
            return FALSE;
        }else{
           
            return  $query->result_array()[0]['property_default_price'];
        }

    }

    public function get_property_prices_on_particular_dates($prize_criteria){
        $date_string_from_to = $this->get_dates_array_of_two_dates(array('from_date'=>$prize_criteria['from_date'],'to_date'=>$prize_criteria['to_date']));
        $prizes = array();
        $default_price = $this->get_property_default_price($prize_criteria['property_id']);
        $prizes['having_custom_price'] = '0';
        foreach($date_string_from_to as $single_date){
            $this->db->select('price');
             $this->db->where('is_active', '1'); 
             $this->db->where('is_delete', '0'); 
             $this->db->where('is_old', '0'); 
             $this->db->where('from_date <=', $single_date); 
             $this->db->where('to_date >=', $single_date); 


             $this->db->where('property_id', $prize_criteria['property_id']); 
             $this->db->limit(1);
             $query = $this->db->get('properties_events_price');
             if ($query->num_rows() == 0)
             {
                $prizes['prices_by_date'][$single_date]=$default_price;
             }else{
                $prizes['prices_by_date'][$single_date]=$query->result_array()[0]['price'];
                $prizes['having_custom_price'] = '1';
                 //return  $query->result_array();
             }
        }
        $prizes['default_price']=$default_price;
        return $prizes;
        
    }

    public function get_property_events_on_particular_dates($prize_criteria){
        

        $query = $this->db->query("select * from properties_events
        where property_id = ".$prize_criteria['property_id']." and is_delete = '0' and is_active = '1' and (from_date BETWEEN CAST('".$prize_criteria['from_date']."' AS DATE) AND CAST('".$prize_criteria['to_date']."' AS DATE) or
        to_date BETWEEN CAST('".$prize_criteria['from_date']."' AS DATE) AND CAST('".$prize_criteria['to_date']."' AS DATE)) ");
        return $query->result_array();


      
        
    }
    
    

    public function check_property_available_on_all_dates($property_id,$from_date,$to_date){
        
        $properties_availability = $this->Property_model->get_properties_availability_on_given_dates_array($property_id,$from_date,$to_date);
        
        $properties_booked_dates = $this->Property_model->get_properties_booked_dates_on_given_dates_array($property_id,$from_date,$to_date);

        if($properties_availability !== 0 && $properties_booked_dates == 0){
            return true;
        }else{
            return false;
        }
    }

    public function get_properties_availability_on_given_dates_array($property_id,$from_date,$to_date){
        $query = $this->db->query("select count(*) from properties_availability
        where property_id = ".$property_id." and is_delete = '0' and (from_date BETWEEN CAST('".$from_date."' AS DATE) AND CAST('".$to_date."' AS DATE) or
        to_date BETWEEN CAST('".$from_date."' AS DATE) AND CAST('".$to_date."' AS DATE))");
        return $query->result();
    }

    public function get_properties_booked_dates_on_given_dates_array($property_id,$from_date,$to_date){
        $date_string_from_to = $this->get_dates_array_of_two_dates(array('from_date'=>$from_date,'to_date'=>$to_date));
        $this->db->where('property_id', $property_id); 
        $this->db->where('is_delete', '0'); 
        $this->db->where('is_active', '1'); 
        $this->db->where_in('date', $date_string_from_to); 
        
        $query = $this->db->get('booking_trip_details');
        return $query->num_rows();
    }

    public function get_property_prices_on_particular_dates_as_string_with_status($prize_criteria){
        $date_string_from_to = $this->get_dates_array_of_two_dates(array('from_date'=>$prize_criteria['from_date'],'to_date'=>$prize_criteria['to_date']));
        $result_set = array();
        
        if($this->check_property_available_on_all_dates($prize_criteria['property_id'],$prize_criteria['from_date'],$prize_criteria['to_date'])){
            

            $total_if_prices_by_date = 0;
            $default_price = $this->get_property_default_price($prize_criteria['property_id']);
            
            foreach($date_string_from_to as $single_date){
                $this->db->select('price');
                $this->db->where('is_active', '1'); 
                $this->db->where('is_delete', '0'); 
                $this->db->where('is_old', '0'); 
                $this->db->where('from_date <=', $single_date); 
                $this->db->where('to_date >=', $single_date); 


                $this->db->where('property_id', $prize_criteria['property_id']); 
                $this->db->limit(1);
                $query = $this->db->get('properties_events_price');
                if ($query->num_rows() == 0)
                {
                    $total_if_prices_by_date += $default_price;
                }else{
                    $total_if_prices_by_date += $query->result_array()[0]['price'];                 
                }
            }
            // $prizes['default_price']=$default_price;
            if($total_if_prices_by_date == 0){
                $result_set['status'] = 'error';
                $result_set['msg'] = 'Price not set.';
            }else{
                $result_set['status'] = 'success';
                $result_set['price'] = $total_if_prices_by_date;
            }

        }else{

            $result_set['status'] = 'error';
            $result_set['msg'] = 'There are few dates that are not available so please check the availablity in the calender.';
            

        }
        return $result_set;
        
    }

    public function get_property_prices_on_particular_dates_as_string($prize_criteria){
        $date_string_from_to = $this->get_dates_array_of_two_dates(array('from_date'=>$prize_criteria['from_date'],'to_date'=>$prize_criteria['to_date']));
        
        $prizes = array();
        $total_if_prices_by_date = 0;
        $default_price = $this->get_property_default_price($prize_criteria['property_id']);
        $prizes['having_custom_price'] = '0';
        foreach($date_string_from_to as $single_date){
            $this->db->select('price');
             $this->db->where('is_active', '1'); 
             $this->db->where('is_delete', '0'); 
             $this->db->where('is_old', '0'); 
             $this->db->where('from_date <=', $single_date); 
             $this->db->where('to_date >=', $single_date); 


             $this->db->where('property_id', $prize_criteria['property_id']); 
             $this->db->limit(1);
             $query = $this->db->get('properties_events_price');
             if ($query->num_rows() == 0)
             {
                 $total_if_prices_by_date += $default_price;
                // $prizes['prices_by_date'][$single_date]=$default_price;
             }else{
                $total_if_prices_by_date += $query->result_array()[0]['price'];
                // $prizes['prices_by_date'][$single_date]=$query->result_array()[0]['price'];
                // $prizes['having_custom_price'] = '1';
                 
             }
        }
        // $prizes['default_price']=$default_price;
        return $total_if_prices_by_date;
        
    }



    public function get_property_price_on_a_particular_date($prize_criteria){
        
        
        $default_price = $this->get_property_default_price($prize_criteria['property_id']);
        
            $this->db->select('price');
             $this->db->where('is_active', '1'); 
             $this->db->where('is_delete', '0'); 
             $this->db->where('is_old', '0'); 
             $this->db->where('from_date <=', $prize_criteria['date']); 
             $this->db->where('to_date >=', $prize_criteria['date']); 


             $this->db->where('property_id', $prize_criteria['property_id']); 
             $this->db->limit(1);
             $query = $this->db->get('properties_events_price');
             if ($query->num_rows() == 0)
             {
                return $default_price;
             }else{
                return $query->result_array()[0]['price'];
             }
        
        
    }



    function get_json_string_of_property_pricing_for_calender_tooltips($property_id){
        $this->db->select('from_date,to_date,price'); 
        $this->db->where('is_delete', '0'); 
        $this->db->where('is_old', '0'); 
        $this->db->where('is_active', '1'); 
        $this->db->where('property_id', $property_id); 
        $query = $this->db->get('properties_events_price');
        $jasonData = '';
        if ($query->num_rows() == 0)
		{
            return FALSE;
        }else{
            
            foreach($query->result() as $property_pricing){
                    // $result_set[] = $property_pricing;      
                    if($jasonData == ''){
                        $jasonData = $this->create_string_with_single_date_n_price($property_pricing);
                      }else{
                        $jasonData .= ','.$this->create_string_with_single_date_n_price($property_pricing);
                      }          
            }
            return  '{'.$jasonData.'}';
        }
    }

    function create_string_with_single_date_n_price($data){
        $period = new DatePeriod(
            new DateTime($data->from_date),
            new DateInterval('P1D'),
            new DateTime(date('Y-m-d', strtotime('+1 day', strtotime($data->to_date))))
       );
// "2018-07-04":"1","2018-07-05":"2","2018-07-06":"3","2018-07-07":"4"
$dates_inner_array_str = '';
       foreach ($period as $key => $value) {
          if($dates_inner_array_str == ''){
            $dates_inner_array_str = '"'.$value->format('Y-m-d').'":"$'.$data->price.'USD"';
          }else{
            $dates_inner_array_str .= ',"'.$value->format('Y-m-d').'":"$'.$data->price.'USD"';
          }
        }
        return $dates_inner_array_str;
    }
/*guest payment history*/
public  function get_guest_payment_history($user_id){
        $this->db->where('users_id', $user_id); 
        $query = $this->db->get('payments');
        if ($query->num_rows() > 0)
        {
                return $query->result();
        }else{
            return 'No result found';
        }
    }
    /*payment refund*/
    public  function get_payment_refund_history($user_id){
        $this->db->where('created_by', $user_id); 
        $query = $this->db->get('payment_refunds');
        if ($query->num_rows() > 0)
        {
                return $query->result();
        }else{
            return 'No result found';
        }
    }
    /*booking properies by user*/
    
/*    public function get_users_features(){
        $query = $this->db->get('users');
        if($query->num_rows() > 0){
            return  $query->result();
        }
        else{
            return 'No Result Found';
        }
    }*/
    /*public function search_beta_loader($search_criteria=null,$count=99,$search_cri_array=array(),$users=null,$dates=null,$start,$end){
        $this->db->limit($start,$end);
        $query = $this->db->get('properties_details');
        print_r($query->result());
    }*/
     public  function property_review($user_id){
        $properties_ids = $this->get_host_properties_ids();
        $this->db->select('*');
        $this->db->from('reviews_ratings');
        $this->db->join('properties_details','reviews_ratings.to_property_id = properties_details.property_id');
        $this->db->join('users','reviews_ratings.by_user_id = users.id');
        $this->db->where_in('reviews_ratings.to_property_id',$properties_ids); 
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
                return $query->result();
        }else{
            return 'No result found';
        }
    }
    public function get_subrub($zipcode)
    {
        $this->db->where('zipcode',$zipcode);
        $query = $this->db->get('opt_loc_zip_suburb');
       if ($query->num_rows() > 0) {
            echo '<option value="-1">Select suburb</option>';
            foreach($query->result() as $res){ ?>
                <option value="<?php echo $res->suburb ?>"><?php echo $res->suburb; ?></option>
           <?php }  
            //echo $res->suburb;  
        }
        else{
            echo '0';
        }
    }
    public function get_zipcode($suburb)
    {
        $this->db->where('suburb',$suburb);
        $query = $this->db->get('opt_loc_zip_suburb');
        if ($query->num_rows() > 0) {
           echo '<option value="-1">Select zipcode</option>';
            foreach($query->result() as $res){
                echo '<option value='.$res->zipcode.'>'.$res->zipcode.'</option>';
            }  
            //echo $res->zipcode; 
        }
        else{
            echo "0";
            //echo 'No zipcode found';
        }
    }

}

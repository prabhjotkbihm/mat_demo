<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cart_model extends CI_Model
{
    public function __construct()

	{

		$this->config->load('ion_auth', TRUE);

        $this->load->database();
        
		$this->load->helper('cookie');

		$this->load->helper('date');

        $this->lang->load('ion_auth');

    }

    public function ajax_confirm_this_booking_by_host($booking_id){
        $logged_in_user=$this->ion_auth->user()->row()->id;
        $my_property_ids_all = $this->Property_model->get_host_properties_ids();
        $my_property_ids = $this->is_this_bookingid_have_my_properties($booking_id,$my_property_ids_all);
        if(is_array($my_property_ids)){
        
            $this->db->where('booking_id', $booking_id);
            $this->db->where_in('property_id',$my_property_ids);
            $this->db->where_not_in('status', array('canceled_by_guest','partial_canceled_by_host'));
            
            if($this->db->update('booking_trip',array('status'=>'confirmed','modified_by'=>$logged_in_user))){
                $this->db->where('booking_id', $booking_id);
                $this->db->where('canceled_by', NULL);
                $this->db->where_in('property_id',$my_property_ids);
                $date = date('Y-m-d H:i:s');
                if($this->db->update('booking_trip_details',array('confirmed_timestamp'=>$date,'modified_by'=>$logged_in_user))){

                    $this->send_emails_for_status_change_booking($booking_id);
                    return 'success';
                        
                    }else{
                        return 'error';
                    }
                     
                    
                }else{
                    return 'error';
                }
            }else{
                return 'error';
            }
       
    }
    public function send_emails_for_status_change_booking($booking_id){
        // $canceled_by = canceled_by_host,canceled_by_guest,canceled_by_admin
        $this->db->select('paid_by_user_id');
        $this->db->where('booking_id', $booking_id);
        $query = $this->db->get('booking_details');
        
        if ($query->num_rows() == 0)
        {
            return false;
        }else{
            $to = $this->ion_auth->user($query->result()[0]->paid_by_user_id)->row()->email;
            $first_name = $this->ion_auth->user($query->result()[0]->paid_by_user_id)->row()->first_name;
            $template_name = 'booking_status_changed';
            $data_for_template =array();
            $subject = 'Booking #'.$booking_id.' is confirmed by host.';
            $message = 'Congratulations!! '.$first_name.',<br>Your booking #'.$booking_id.' is confirmed by host.';
            $this->ion_auth->send_mail($to,$template_name,$subject,$data_for_template,$message);
        }    



    }



    public function cancel_booking_by_guest($booking_id,$reason_behind_cancel_booking,$notes_if_any){
        $paid_by_user_id=$this->ion_auth->user()->row()->id;
        $this->db->where('booking_id', $booking_id);
        $this->db->where('paid_by_user_id', $paid_by_user_id);
        if($this->db->update('booking_details',array('status'=>'canceled_by_guest','modified_by'=>$paid_by_user_id))){
            $this->db->where('booking_id', $booking_id);
            $this->db->where('book_by_user_id', $paid_by_user_id);
            if($this->db->update('booking_trip',array('status'=>'canceled_by_guest','modified_by'=>$paid_by_user_id))){

                $this->db->where('booking_id', $booking_id);
                $this->db->where('book_by_user_id', $paid_by_user_id);
                if($this->db->update('booking_trip_details',array('canceled_by'=>$paid_by_user_id,'modified_by'=>$paid_by_user_id))){
  
                    if($this->insert_refund_entry($booking_id,$reason_behind_cancel_booking,$notes_if_any,'canceled_by_guest',NULL)){
                        
                        $this->send_emails_for_cancel_booking('canceled_by_guest',$booking_id,$reason_behind_cancel_booking,$notes_if_any);

                        return 'success';
                    }else{
                        return 'error';
                    }
                }else{
                    return 'error';
                }
            }else{
                return 'error';
            }
        }else{
            return 'error';
        }
                       
    }

    public function send_emails_for_cancel_booking($canceled_by,$booking_id,$reason_behind_cancel_booking,$notes_if_any){
        // $canceled_by = canceled_by_host,canceled_by_guest,canceled_by_admin
        $this->db->select('paid_by_user_id');
        $this->db->where('booking_id', $booking_id);
        $query = $this->db->get('booking_details');
        
        if ($query->num_rows() == 0)
        {
            return false;
        }else{
            $to = $this->ion_auth->user($query->result()[0]->paid_by_user_id)->row()->email;
            $first_name = $this->ion_auth->user($query->result()[0]->paid_by_user_id)->row()->first_name;
            $template_name = 'mail_plain';
            

            $data_for_template =array();
            $data_for_template['reason'] = $reason_behind_cancel_booking;
            $data_for_template['notes'] = $notes_if_any;
            $data_for_template['status'] = $canceled_by;
            $message = 'Booking #'.$booking_id.' Canceled with status of '.$canceled_by.' <br> ';
            $message .= 'Reason: '.$reason_behind_cancel_booking.'<br>Notes: '.$notes_if_any.' <br> ';


            if($canceled_by == 'canceled_by_host'){
                $template_name = 'cancel_booking_by_host_mail_to_guest';
                $subject = 'Booking #'.$booking_id.' Canceled by host';
                $message = 'Sorry!! '.$first_name.',<br /> Your Booking #'.$booking_id.' Canceled by the Property host. <br>';
                $message .= 'Reason Mentioned: '.$reason_behind_cancel_booking.'<br>Notes(if any): '.$notes_if_any.' <br> ';
            }elseif($canceled_by == 'canceled_by_guest'){
                $template_name = 'cancel_booking_by_guest_mail_to_guest';
                $subject = 'Booking #'.$booking_id.' Canceled by Guest';
                $message = 'Hi!! '.$first_name.',<br /> You have just canceled your Booking #'.$booking_id.' successfully.<br >';
                $message .= 'Reason mentioned by you: '.$reason_behind_cancel_booking.'<br>Notes(if any): '.$notes_if_any.' <br> ';
            }elseif($canceled_by == 'canceled_by_admin'){
                $subject = 'Booking #'.$booking_id.' Canceled by Admin';
            }else{
                $subject = 'Booking #'.$booking_id.' Canceled';
            }
            
            $this->ion_auth->send_mail($to,$template_name,$subject,$data_for_template,$message);
        }    


        $this->db->select('property_id');
        $this->db->where('booking_id', $booking_id);
        $this->db->group_by('property_id'); 
        $query = $this->db->get('booking_trip');
        
        if ($query->num_rows() == 0)
        {
            return false; 
        }else{
            foreach($query->result_array() as $property_id_obj){
                $added_by_prop = $this->Property_model->get_property_details($property_id_obj['property_id'])->property_added_by;
                $to = $this->ion_auth->user($added_by_prop)->row()->email;
                $first_name = $this->ion_auth->user($added_by_prop)->row()->first_name;
                $template_name = 'mail_plain';
                $data_for_template =array();
                $data_for_template['reason'] = $reason_behind_cancel_booking;
                $data_for_template['notes'] = $notes_if_any;
                $data_for_template['status'] = $canceled_by;
                $message = 'Booking #'.$booking_id.' Canceled with status of '.$canceled_by.' <br> ';
                $message .= 'Reason: '.$reason_behind_cancel_booking.'<br>Notes: '.$notes_if_any.' <br> ';
                if($canceled_by == 'canceled_by_host'){
                    $template_name = 'cancel_booking_by_host_mail_to_host';
                    $subject = 'Booking #'.$booking_id.' Canceled by host';

                    $message = 'Hi!! '.$first_name.',<br /> You have successfully canceled a Booking #'.$booking_id.'<br> ';
                    $message .= 'Reason you mentioned: '.$reason_behind_cancel_booking.'<br>Notes: '.$notes_if_any.' <br> ';

                }elseif($canceled_by == 'canceled_by_guest'){
                    $template_name = 'cancel_booking_by_guest_mail_to_host';
                    $subject = 'Booking #'.$booking_id.' Canceled by Guest';
                    $message = 'Sorry!! '.$first_name.',<br />Booking #'.$booking_id.' is canceled by the guest.<br> ';
                    $message .= 'Reason: '.$reason_behind_cancel_booking.'<br>Notes: '.$notes_if_any.' <br> ';

                }elseif($canceled_by == 'canceled_by_admin'){
                    $subject = 'Booking #'.$booking_id.' Canceled by Admin';
                }else{
                    $subject = 'Booking #'.$booking_id.' Canceled';
                }
                
                $this->ion_auth->send_mail($to,$template_name,$subject,$data_for_template,$message);
            }
            
        }    

    }



    public function cancel_booking_by_host($booking_id,$reason_behind_cancel_booking,$notes_if_any){
        $logged_in_user=$this->ion_auth->user()->row()->id;
        $my_property_ids_all = $this->Property_model->get_host_properties_ids();
        $my_property_ids = $this->is_this_bookingid_have_my_properties($booking_id,$my_property_ids_all);
        if(is_array($my_property_ids)){
        $this->db->where('booking_id', $booking_id);
        // $this->db->where_in('property_id',$my_property_ids);
        $this->db->where('status !=', 'canceled_by_guest');

        if($this->db->update('booking_details',array('status'=>'partial_canceled_by_host','modified_by'=>$logged_in_user))){
            $this->db->where('booking_id', $booking_id);
            $this->db->where_in('property_id',$my_property_ids);
            $this->db->where('status !=', 'canceled_by_guest');
            
            if($this->db->update('booking_trip',array('status'=>'partial_canceled_by_host','modified_by'=>$logged_in_user))){
                $this->db->where('booking_id', $booking_id);
                $this->db->where_in('property_id',$my_property_ids);
                
                if($this->db->update('booking_trip_details',array('canceled_by'=>$logged_in_user,'modified_by'=>$logged_in_user))){
                    $refund_result = false;
                    foreach($my_property_ids as $my_property_id){
                        $refund_result = $this->insert_refund_entry($booking_id,$reason_behind_cancel_booking,$notes_if_any,'partial_canceled_by_host',$my_property_id);
                        
                    }
                     
                    if($refund_result){
                        
                                
                        $this->send_emails_for_cancel_booking('canceled_by_host',$booking_id,$reason_behind_cancel_booking,$notes_if_any);

                        return 'success';
                    }else{
                        return 'error';
                    }
                }else{
                    return 'error';
                }
            }else{
                return 'error';
            }
        }else{
            return 'error';
        }
    }else{
        return 'error';
    }
                       
    }



    public function is_this_bookingid_have_my_properties($booking_id,$my_property_ids){
        $result_set = array();
        $this->db->select('property_id');
        $this->db->where('booking_id', $booking_id); 
        $this->db->where_in('property_id',$my_property_ids);
                
        $query = $this->db->get('booking_trip');
        
        if ($query->num_rows() == 0)
        {
            return false;
        }else{
            foreach($query->result_array() as $property_id_obj){
                $result_set[] = $property_id_obj['property_id'];
            }
            return $result_set;
        }
    }

    

    public function insert_refund_entry($booking_id,$reason_behind_cancel_booking,$notes_if_any,$refund_reason,$property_id=NULL){
        $this->db->where('booking_detail_id', $booking_id); 
        if($property_id!==NULL){
            $this->db->where('property_id', $property_id); 
        }
                
        $query = $this->db->get('payment_refunds');
        
        if ($query->num_rows() == 0)
        {
            if($property_id!==NULL){
                $data['property_id'] =  $property_id;
            }
            $data['booking_detail_id'] =  $booking_id;
            $data['refund_reason'] =  $refund_reason;
            $data['reason_behind_cancel_booking'] =  $reason_behind_cancel_booking;
            $data['notes_if_any'] =  $notes_if_any;
            $data['created_by'] =  $this->ion_auth->user()->row()->id;
            $data['modified_by'] =  $this->ion_auth->user()->row()->id;

            $this->db->insert('payment_refunds', $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }else{
            return false;
            
        } 



        
    }

    public function add_to_wishlist_n_cart($cart_data,$notes){
        $cart_data['user_id']=$this->ion_auth->user()->row()->id;
        $cart_data['created_by']=$this->ion_auth->user()->row()->id;
        $wishlist_id = $this->add_to_wishlist($cart_data);
         if($wishlist_id){
            //$cart_data['wishlist_id']=$wishlist_id;
            $cart_data['notes'] = $notes;
            return $this->add_to_session($wishlist_id,$cart_data);
         }
    }

    public function add_to_wishlist($data){
        $this->db->insert('wishlist', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function add_to_session($wishlist_id,$cart_data){
        $existing_cart = array();
        $existing_cart = $this->session->userdata('cart');
        $existing_cart[$wishlist_id] = $cart_data;
        $this->session->unset_userdata('cart');
        $this->session->set_userdata('cart',$existing_cart);
        return true;
    }

    public function remove_this_cart_item($cartItemId){
        $existing_cart = array();
        $existing_cart = $this->session->userdata('cart');
        unset($existing_cart[$cartItemId]);
        $this->session->set_userdata('cart',$existing_cart);
        return $this->remove_this_cart_item_from_wishlist($cartItemId);
        
    }

    public function remove_this_cart_item_from_wishlist($wishlist_id){
        $this->db->where('wishlist_id', $wishlist_id);
        
        if($this->db->update('wishlist',array('is_delete'=>'1'))){
            return TRUE;
        }else{
            return FALSE;
        }
    }


    public function insertTransaction($data = array()){

        if($this->is_transaction_saved($data['txn_id'])){
            return 'already_exist';
        }else{
            
            $insert = $this->db->insert('payments',$data);
            if($insert){
                $BookingData['amount_paid']=$data['payment_gross'];
                $BookingData['method']= $data['type'];//'paypal';
                $BookingData['payment_data'] = "{'paypal_id':'".$data['txn_id']."'}";
                $BookingData['payment_id']=$this->db->insert_id();
                $BookingData['paid_by_user_id']=$data['users_id'];
                $BookingData['created_by']=$data['users_id'];
                $BookingData['modified_by']=$data['users_id'];
                $insertBooking = $this->insertBooking($BookingData);
                

                $this->session->unset_userdata('cart');
                return 'added';
            }else{
                return 'failed';
            }
        }
    }
    public function insert_cards($card_data = array()){
        if($this->is_transaction_saved($card_data['txn_id'])){
            return 'already_exist';
        }else{
            $card_insert = $this->db->insert('cards_data',$card_data);
        }          
    }

    public function is_transaction_saved($txn_id){
        $this->db->where('txn_id', $txn_id); 
                
        $query = $this->db->get('payments');
        
        if ($query->num_rows() == 0)
        {
            return false;
        }else{
            return true;
            
        } 
    }

    public function insertBooking($data=array()){
        $insert = $this->db->insert('booking_details',$data);
        $booking_id_added=$this->db->insert_id();
        if($insert){
            $commissionDataArray = array();
            $emails['tohost'] = array();
            $emails['toguest'] = array();
            foreach($_SESSION['cart'] as $cartItemKey=>$cartItemVal){
                $BookingTripData = array();
                $BookingTripData['booking_id']=$booking_id_added;
                $BookingTripData['book_by_user_id']=$data['paid_by_user_id'];
                $BookingTripData['created_by']=$data['paid_by_user_id'];
                $BookingTripData['modified_by']=$data['paid_by_user_id'];

                $BookingTripData['property_id'] = $cartItemVal['property_id'];
                $BookingTripData['from_date'] = $cartItemVal['from_date'];
                $BookingTripData['to_date'] = $cartItemVal['to_date'];
                $BookingTripData['notes'] = $cartItemVal['notes'];

                $BookingTripResult = $this->db->insert('booking_trip', $BookingTripData);

                $booking_trip_id=$this->db->insert_id();
                
                $property_detail = $this->Property_model->get_property_details($cartItemVal['property_id']);
                
                $current_rating_of_host = $this->ion_auth->get_current_rating_of_user($property_detail->property_added_by);

                if($current_rating_of_host['rating'] >= 3){
                    //$comm_cut_percentage = 13;
                    $comm_cut_percentage = 3;
                }else{
                    $comm_cut_percentage = 4;
                    //$comm_cut_percentage = 14;
                }

                $total_amount = number_format((float)$this->get_property_prices_on_particular_dates_as_int($cartItemVal['property_id'],$cartItemVal['from_date'],$cartItemVal['to_date']), 1, '.', '');
                
                /*admin commission*/
                $actual_price = $total_amount * (100 / 110);
                $host_actual_commis = $actual_price * ($comm_cut_percentage / 100);
                $admin_commission1 = ($total_amount-$actual_price)+$host_actual_commis;
                $admin_commission = round($admin_commission1, 2);
                /*host commission*/
                $my_commission1 = $total_amount - $admin_commission;
                $my_commission = round($my_commission1, 2);
                /*$admin_commission = ($comm_cut_percentage / 100) * $total_amount;
                $my_commission =$total_amount - $admin_commission;*/

                $commissionData = array();
                $commissionData['booking_id']=$booking_id_added;
                $commissionData['booking_trip_id']=$booking_trip_id;
                $commissionData['comm_cut_percentage']=$comm_cut_percentage;
                $commissionData['comm_paid_to']=$property_detail->property_added_by;
                $commissionData['is_payment_success']='1';
                $commissionData['payment_id']=$data['payment_id'];
                $commissionData['can_withdraw_after']=$cartItemVal['to_date'].' 23:59:59';
                $commissionData['total_amount']=$total_amount;
                $commissionData['my_commission']=$my_commission;
                $commissionData['admin_commission']=$admin_commission;
                $commissionData['rating_at_that_time']=$current_rating_of_host['rating'];
                $commissionData['property_id']=$cartItemVal['property_id'];
                $commissionData['created_by']=$this->ion_auth->user()->row()->id;
                $commissionData['modified_by']=$this->ion_auth->user()->row()->id;
                
                // $commissionDataArray[] = $commissionData;

                if(!isset($emails['tohost'][$property_detail->property_added_by])){
                    $emails['tohost'][$property_detail->property_added_by] = array();
                }
                if(!isset($emails['toguest'][$data['paid_by_user_id']])){
                    $emails['toguest'][$data['paid_by_user_id']] = array();
                }

                $property_booking_array_for_email = array();
                $property_booking_array_for_email['booking_id'] = $booking_id_added;
                $property_booking_array_for_email['property_id']=$cartItemVal['property_id'];
                $property_booking_array_for_email['property_details']=$this->Property_model->get_property_name_n_image_by_id($cartItemVal['property_id']);
                
                $property_booking_array_for_email['from_date'] = $cartItemVal['from_date'];
                $property_booking_array_for_email['to_date'] = $cartItemVal['to_date'];
                $property_booking_array_for_email['notes'] = $cartItemVal['notes'];
               
               
                $emails['tohost'][$property_detail->property_added_by][] = $property_booking_array_for_email;
                $emails['toguest'][$data['paid_by_user_id']][] = $property_booking_array_for_email;
                

                $insert_booking_commision = $this->db->insert('booking_commision',$commissionData);
                

                
                
                // Make Trip Details by single single dates
                $dates_tomake_array['from_date'] = $cartItemVal['from_date'];
                $dates_tomake_array['to_date'] = $cartItemVal['to_date'];
                $dates_array = $this->Property_model->get_dates_array_of_two_dates($dates_tomake_array);
                $BookingTripDetails = array();
                foreach($dates_array as $dates_array_sin){
                    $BookingTripDetailsData = array();
                    $BookingTripDetailsData['booking_trip_id']=$booking_trip_id;
                    $BookingTripDetailsData['booking_id'] = $BookingTripData['booking_id'];
                    $BookingTripDetailsData['property_id'] = $cartItemVal['property_id'];
                    $BookingTripDetailsData['book_by_user_id'] = $data['paid_by_user_id'];
                    $BookingTripDetailsData['date'] = $dates_array_sin;

                    $array_for_check_price_on_particular_date['property_id'] = $cartItemVal['property_id'];
                    $array_for_check_price_on_particular_date['date'] = $dates_array_sin;

                    $BookingTripDetailsData['price_at_the_time_of_booking'] = $this->Property_model->get_property_price_on_a_particular_date($array_for_check_price_on_particular_date);
                    $BookingTripDetailsData['created_by']=$data['paid_by_user_id'];
                    $BookingTripDetailsData['modified_by']=$data['paid_by_user_id'];
                    $BookingTripDetails[] = $BookingTripDetailsData;
                }
                
                $this->db->insert_batch('booking_trip_details', $BookingTripDetails); 
                
                // $addCommsion = $this->insertCommision($commissionDataArray);

            }
            $this->send_booking_emails($emails);
            // $this->db->insert_batch('booking_commision', $commissionDataArray); 
            
            return 'added';
        }else{
            return 'failed';
        }
    }

    public function send_booking_emails($emails){
        // echo "<pre>"; print_r($emails);echo "</pre>";
        if(is_array($emails)){
            foreach($emails as $UserType=>$emailVal){
                foreach($emailVal as $UserID=>$HostemailValVal){
                    $to = $this->ion_auth->user($UserID)->row()->email;
                    
                    $data_for_template = $HostemailValVal;
                    if($UserType == 'tohost'){
                        $template_name = 'booking_new_host';
                        // $template_name = 'mail_plain';
                        $subject = 'You got new booking';
                        
                        $message = "Hi host you have a new booking";
                    
                    }elseif($UserType == 'toguest'){
                        $template_name = 'booking_new_guest';
                        // $template_name = 'mail_plain';
                        $subject = 'New booking placed successfully.';
                        
                        $message = "Hi Guest your new booking successfully placed.";
                    }else{
                        return false;
                    }
                   // echo "To:".$to;die('sadasd');
                    $this->ion_auth->send_mail($to,$template_name,$subject,$data_for_template,$message);
                }
            }
        }else{
            return false;
        }
    }

    public function get_property_prices_on_particular_dates_as_int($property_id,$from_date,$to_date){
        $prize_criteria['property_id'] = $property_id;
        $prize_criteria['from_date'] = $this->ion_auth->change_date_format($from_date);
        $prize_criteria['to_date'] = $this->ion_auth->change_date_format($to_date);
        
        
        $prize = $this->Property_model->get_property_prices_on_particular_dates_as_string_with_status($prize_criteria);
        if($prize['status'] == 'success'){
            $msg = $prize['price'];
            return $prize['price'];
            // echo "Total Amount to be charged $".$msg."USD";
        }else{
           return 'N';
        }
        
    }
    /*checkout update*/
     public function update_add_to_wishlist_n_cart($cart_data,$notes){
            $cart_data['user_id']=$this->ion_auth->user()->row()->id;
            $cart_data['created_by']=$this->ion_auth->user()->row()->id;
            $cart_data['from_date'] = $this->ion_auth->change_date_format_with_english_month($this->input->post('from_date'));
            $cart_data['to_date'] = $this->ion_auth->change_date_format_with_english_month($this->input->post('to_date'));
            $modified_date = date('Y-m-d h:i:s');
            $wishlist_id = $cart_data['wish_id'];
            $this->db->where('wishlist_id', $wishlist_id);
            if($this->db->update('wishlist',array('from_date'=>$cart_data['from_date'],'to_date' => $cart_data['to_date'],'modified_date' => $modified_date ))){
                $cart_data['notes'] = $notes;
                return $this->add_to_session($wishlist_id,$cart_data);
             }
        }
}



?>